package Part1;

public class GameDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Create new Game object and use setters to set the fields
		Game doom = new Game();
		doom.setGame("Doom");
		doom.setMaxPlayers(30);
		
		//Create new GameWithTimeLimit object and use setters to set the fields
		GameWithTimeLimit callOfDuty = new GameWithTimeLimit();
		callOfDuty.setGame("Call of Duty");
		callOfDuty.setMaxPlayers(10);
		callOfDuty.setTimeLimit(15);
		
		
		//Print both class' toString() methods
		System.out.println(doom + "\n");
		System.out.println(callOfDuty);
		
		
	}

}
