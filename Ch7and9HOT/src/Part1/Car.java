package Part1;

public class Car extends Vehicle{

	//Overrides the superclass accelerate() method to increase speed by 10
	@Override
	
	public void accelerate() {
		speed += 10;
	}
	
}
