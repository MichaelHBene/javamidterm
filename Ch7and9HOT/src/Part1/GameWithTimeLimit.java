package Part1;

public class GameWithTimeLimit extends Game{
	//Int which holds the time limit of the game
	private int timeLimit;
	
	//Setter for the timeLimit field
	public void setTimeLimit(int mins) {
		timeLimit = mins;
	}
	
	//Getter for the timeLimit field
	public int getTimeLimit() {
		return timeLimit;
	}
	
	//Overrides the toString method to include time limit in the output
	public String toString() {
		return "You are currently playing:\n" + super.getGame() + "\nTime Limit: " + getTimeLimit() + "\nMax Players: " + super.getMaxPlayers();
	}
}
