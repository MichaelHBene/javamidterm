package Part1;

public class Truck extends Vehicle{
	
	//Overrides the superclass accelerate() method to increase speed by 3
	@Override
	
	public void accelerate() {
		speed += 3;
	}
}
