package Part1;

public class Vehicle {
	//int which holds the speed of the vehicle
	protected int speed;
	
	//public method which increases the speed by 5 when called
	public void accelerate() {
		speed += 5;
	}
	
	//returns the speed field
	public int getSpeed() {
		return speed;
	}
	
	
}
