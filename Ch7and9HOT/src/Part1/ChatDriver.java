package Part1;

import java.util.Scanner;
public class ChatDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Creates two arrays, one int array for area code and one double array for per-minute rates.
		int[] areaCode = {262,414,608,715,815,920};
		double[] perMinuteRate = {.07,.10, .05, .16, .24, .14};
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Please enter your area code:");
		int ac = keyboard.nextInt();
		
		System.out.println("Please enter the length of your call in minutes:");
		
		int min = keyboard.nextInt();
		
		double chatCost = 0;
		
		//Searches the areaCode array to see if the user input matches one in the array. If so, it will calculate the cost of the chat from the rate variable stored in the same subscript in the double array.
		for(int x = 0; x < areaCode.length; ++x) {
			if(ac == areaCode[x]) {
				chatCost = (min * perMinuteRate[x]);
			}
		}
		
		System.out.printf("Your total cost for area code %d for %d minutes is: $%.2f", ac, min, chatCost);
	}

}
