package Ch10Challenge5;

public class InvalidPayRate extends Exception{
	public InvalidPayRate() {
		super("Error. Invalid pay rate");
	}
}
