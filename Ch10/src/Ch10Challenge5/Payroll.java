package Ch10Challenge5;

public class Payroll {
	private String name;
	private int id;
	private double payRate;
	private double hoursWorked;
	
	public Payroll(String employeeName, int employeeID) throws InvalidEmployeeName, InvalidEmployeeID {
		if(employeeName == "") {
			throw new InvalidEmployeeName();
		}
		else {
			name = employeeName;	
		}
		if(employeeID <= 0) {
			throw new InvalidEmployeeID();
		}
		else {
			id = employeeID;	
		}
	}
	public String getName() {
		return name;
	}
	public int getID() {
		return id;
	}
	public void setPayRate(double employeeRate) throws InvalidPayRate {
		if(employeeRate < 0 || employeeRate > 25) {
			throw new InvalidPayRate();
		}
		else {
			payRate = employeeRate;	
		}
	}
	public double getPayRate() {
		return payRate;
	}
	public void setHours(double employeeHours) throws InvalidEmployeeHours {
		if(employeeHours < 0 || employeeHours > 84) {
			throw new InvalidEmployeeHours();
		}
		else {
			hoursWorked = employeeHours;	
		}
	}
	public double getHours() {
		return hoursWorked;
	}

	public double getGrossPay() {
		double grossPay = hoursWorked * payRate;
		return grossPay;
	}
}	
