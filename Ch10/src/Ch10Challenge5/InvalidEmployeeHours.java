package Ch10Challenge5;

public class InvalidEmployeeHours extends Exception{
	public InvalidEmployeeHours() {
		super("Error. Invalid employee hours.");
	}
}
