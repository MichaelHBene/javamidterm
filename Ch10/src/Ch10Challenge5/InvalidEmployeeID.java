package Ch10Challenge5;

public class InvalidEmployeeID extends Exception{
	public InvalidEmployeeID() {
		super("Error. Invalid employee ID");
	}
}
