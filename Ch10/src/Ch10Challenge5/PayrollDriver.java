package Ch10Challenge5;

public class PayrollDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			Payroll emp1 = new Payroll("Greg", 1111111);
			emp1.setHours(4.7);
			emp1.setPayRate(12);
		
			System.out.println(emp1.getName() + "\n Employee #" + emp1.getID() + "\n Hours Worked: " + emp1.getHours() + "\n Hourly Rate: " + emp1.getPayRate() + "\n Gross Pay: " + emp1.getGrossPay());
		}
		catch(InvalidEmployeeName | InvalidEmployeeHours | InvalidEmployeeID | InvalidPayRate e){
			System.out.println(e.getMessage());
		}
	}

}
