package Ch10Challenge8;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class FileDecryptionDriver {
	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		DataInputStream inputFile = new DataInputStream(new FileInputStream("src\\Ch10Challenge8\\EncryptedFile.dat"));
		DataOutputStream outputFile = new DataOutputStream(new FileOutputStream("src\\Ch10Challenge8\\DecryptedFile.dat"));
		while (true) {
			try {
				outputFile.writeChar(inputFile.readChar() - 3);
			}
			catch(EOFException e) {
				System.out.println("File decrypted.");
				break;
			}
		}
		inputFile.close();
		outputFile.close();
	}
}
