package Ch10Challenge6;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileArray {
	
	public static void writeArray(String fileName, int[] array) throws IOException {
		DataOutputStream outputFile = new DataOutputStream(new FileOutputStream(fileName));
		for(int x = 0; x < array.length; ++x) {
			outputFile.writeInt(array[x]);
		}
		outputFile.close();
	}
	
	public static void readArray(String fileName, int[] array) throws IOException {
		DataInputStream inputFile = new DataInputStream(new FileInputStream(fileName));
		boolean endOfFile = false;
		int number;
		while (!endOfFile) {
			try {
				for(int x = 0; x < array.length; ++x) {
					number = inputFile.readInt();
					System.out.println(number + " ");
				}
			}
			catch(EOFException e) {
				endOfFile = true;
			}
		}
		
		inputFile.close();
		
	}
}
