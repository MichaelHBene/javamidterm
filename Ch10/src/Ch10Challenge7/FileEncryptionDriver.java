package Ch10Challenge7;

import java.io.*;
import java.util.Scanner;

public class FileEncryptionDriver {

	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		
		DataOutputStream unencryptedFile = new DataOutputStream(new FileOutputStream("src\\Ch10Challenge7\\UnencryptedFile.dat"));
		unencryptedFile.writeChars("Hello World");
		
		DataInputStream inputFile = new DataInputStream(new FileInputStream("src\\Ch10Challenge7\\UnencryptedFile.dat"));
		
		DataOutputStream encryptedFile = new DataOutputStream(new FileOutputStream("src\\Ch10Challenge7\\EncryptedFile.dat"));
		
		while(true) {
			try {
				
				encryptedFile.writeChar(inputFile.readChar() + 3);
				
			}
			
			catch(EOFException e) {
				System.out.println("File encrypted.");
				break;
			}	
		}

		unencryptedFile.close();
		encryptedFile.close();
	}

}
