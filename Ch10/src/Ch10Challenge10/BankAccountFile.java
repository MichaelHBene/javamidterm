package Ch10Challenge10;

import java.io.*;

public class BankAccountFile {
	private final int RECORD_SIZE = 44;
	private RandomAccessFile accountFile;
	
	public BankAccountFile(String filename) throws FileNotFoundException{
		accountFile = new RandomAccessFile(filename, "rw");
	}
	
	public void writeBankAccount(BankAccount account) throws IOException{
		double balance =  account.getBalance();
		double interest = account.getInterest();
		
		accountFile.writeDouble(balance);
		accountFile.writeDouble(interest);
		
	}
	
	public BankAccount readBankAccount() throws IOException{
		double balance = accountFile.readDouble();
		double interestRate = accountFile.readDouble();
		BankAccount account = new BankAccount(balance, interestRate);
		return account;
	}
	
	/*
	private long getByteNum(long recordNum) {
		return
	}
	*/
	
}
