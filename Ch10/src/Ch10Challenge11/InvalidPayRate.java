package Ch10Challenge11;

public class InvalidPayRate extends Exception{
	public InvalidPayRate() {
		super("Error. Invalid Payrate.");
	}
}
