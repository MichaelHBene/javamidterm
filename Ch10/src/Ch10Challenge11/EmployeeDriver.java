package Ch10Challenge11;

public class EmployeeDriver {

	public static void main(String[] args){
		// TODO Auto-generated method stub
		try{
			ProductionWorker emp = new ProductionWorker("Craig", "111-M", "11/9/2018", 1, 5.5);
			
			System.out.println(emp.toString());	
		}
		catch(InvalidEmployeeNumber | InvalidShift | InvalidPayRate e) {
			System.out.println(e.getMessage());
		}
	}

}
