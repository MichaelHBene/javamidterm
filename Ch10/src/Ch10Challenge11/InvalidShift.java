package Ch10Challenge11;

public class InvalidShift extends Exception{
	public InvalidShift() {
		super("Error. Invalid Shift.");
	}
}
