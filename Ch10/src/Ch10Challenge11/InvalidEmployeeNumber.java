package Ch10Challenge11;

public class InvalidEmployeeNumber extends Exception{
	public InvalidEmployeeNumber() {
		super("Error. Invalid Employee Number Format.");
	}

}
