package Ch10Challenge3;

public class NegativeRetailPrice extends Exception{
	public NegativeRetailPrice() {
		super("Error. The retail price of an item cannot be negative.");
	}
}
