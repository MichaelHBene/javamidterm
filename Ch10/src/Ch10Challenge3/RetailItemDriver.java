package Ch10Challenge3;

public class RetailItemDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		RetailItem item1 = new RetailItem();
		RetailItem item2 = new RetailItem();
		RetailItem item3 = new RetailItem();
		
		try {	
			item1.setDescription("Jacket");
			item1.setUnits(12);	
			item1.setPrice(59.95);	
		
			item2.setDescription("Designer Jeans");
			item2.setUnits(40);
			item2.setPrice(34.95);
		
			item3.setDescription("Shirt");
			item3.setUnits(20);
			item3.setPrice(24.95);
			
			System.out.println("          Description       Units on Hand         Price");
			System.out.println("=======================================================");
			System.out.println("Item #1     " + item1.getDescription() + "               " + item1.getUnits() + "              $" + item1.getPrice());
			System.out.println("Item #2     " + item2.getDescription() + "       " + item2.getUnits() + "              $" + item2.getPrice());
			System.out.println("Item #3     " + item3.getDescription() + "                " + item3.getUnits() + "              $" + item3.getPrice());
		
		}
		catch(NegativeRetailPrice | NegativeItemNumber e) {
			System.out.println(e.getMessage());
		}
	}

}
