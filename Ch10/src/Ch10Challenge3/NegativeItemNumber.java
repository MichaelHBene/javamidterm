package Ch10Challenge3;

public class NegativeItemNumber extends Exception {
	public NegativeItemNumber() {
		super("Error. The number of items cannot be negative.");
	}
}
