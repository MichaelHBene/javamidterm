package Ch10Challenge3;

public class RetailItem {
	private String description;
	private int unitsOnHand;
	private double price;
	
	public void setDescription(String item) {
		description = item;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setUnits(int numUnits) throws NegativeItemNumber{
		if(numUnits < 0) {
			throw new NegativeItemNumber();
		}
		else {
			unitsOnHand = numUnits;	
		}
	}
	
	public int getUnits() {
		return unitsOnHand;
	}
	
	public void setPrice(double cost) throws NegativeRetailPrice {
		if(cost < 0) {
			throw new NegativeRetailPrice();
		}
		else {
			price = cost;	
		}
	}
	
	public double getPrice() {
		return price;
	}
	
	
}
