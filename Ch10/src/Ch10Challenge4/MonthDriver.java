package Ch10Challenge4;

public class MonthDriver {

	public static void main(String[] args) throws InvalidMonthNumber, InvalidMonthName {
		// TODO Auto-generated method stub
		try {
			Month month1 = new Month(1);
			System.out.println(month1.getMonthNumber());
			System.out.println(month1.getMonthName());
			
			Month month2 = new Month("january");
			System.out.println(month2.getMonthNumber());
			System.out.println(month2.toString());
			System.out.println(month1.equals(month2));
			System.out.println(month1.lessThan(month2));
			
			Month month3 = new Month();
			System.out.println(month3.getMonthNumber());
			System.out.println(month3.getMonthName());	
		}
		
		catch( InvalidMonthNumber | InvalidMonthName e) {
			System.out.println(e.getMessage());
		}
	}

}
