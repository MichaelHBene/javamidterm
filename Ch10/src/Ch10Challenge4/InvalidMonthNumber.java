package Ch10Challenge4;

public class InvalidMonthNumber extends Exception{
	public InvalidMonthNumber() {
		super("Error. Invalid month number.");
	}
}
