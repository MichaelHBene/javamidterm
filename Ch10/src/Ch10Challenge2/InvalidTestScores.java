package Ch10Challenge2;

public class InvalidTestScores extends Exception{
	public InvalidTestScores() {
		super("Error. The average test scores cannot be negative or greater than 100.");
	}
	
}
