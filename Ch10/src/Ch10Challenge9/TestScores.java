package Ch10Challenge9;

import java.io.Serializable;

public class TestScores implements Serializable {
	private int[] testScores;
	
	public TestScores(int[] scores) {
		testScores = scores;
	}
	
	public double getAverage() {
		int totalScores = 0;
		for(int x = 0; x < testScores.length; ++x) {
			totalScores += testScores[x];
		}
		double avgScores = (double)totalScores / (double)testScores.length;
		
		if(avgScores < 100) {
			throw new IllegalArgumentException("The average scores cannot be less than 100.");
		}
		else {
			return avgScores;
		}
		
	}
	
	
}
