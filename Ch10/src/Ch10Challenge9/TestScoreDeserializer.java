package Ch10Challenge9;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class TestScoreDeserializer {

	public static void main(String[] args) throws FileNotFoundException, IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		
		ObjectInputStream objectInputFile = new ObjectInputStream(new FileInputStream("src\\Ch10Challenge9\\TestScores.dat"));
		
		TestScores[] scores = new TestScores[5];
		
		for(int i = 0; i < scores.length; ++i) {
			scores[i] = (TestScores) objectInputFile.readObject();
			System.out.println(scores[i].getAverage());
		}
		
		objectInputFile.close();
		
	}

}
