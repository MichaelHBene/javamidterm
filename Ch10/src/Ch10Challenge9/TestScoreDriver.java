package Ch10Challenge9;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class TestScoreDriver {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		// TODO Auto-generated method stub
		int[] scores1 = {190,190,190,190,190};
		int[] scores2 = {185,185,185,185,185};
		int[] scores3 = {180,180,180,180,180};
		int[] scores4 = {175,175,175,175,175};
		int[] scores5 = {170,170,170,170,170};
		
		
		TestScores test1 = new TestScores(scores1);
		TestScores test2 = new TestScores(scores2);
		TestScores test3 = new TestScores(scores3);
		TestScores test4 = new TestScores(scores4);
		TestScores test5 = new TestScores(scores5);
		
		TestScores[] testScores = {test1, test2, test3, test4, test5};
		
		ObjectOutputStream objectOutputFile = new ObjectOutputStream(new FileOutputStream ("src\\Ch10Challenge9\\TestScores.dat"));
		
		for(int i = 0; i < testScores.length; ++i) {
			objectOutputFile.writeObject(testScores[i]);
		}
		
		objectOutputFile.close();

	}

}
