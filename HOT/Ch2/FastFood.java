import java.util.Scanner;
public class FastFood
{
    public static void main (String[] args)
    {
        //Creates a new scanner object for user input
        Scanner keyboard = new Scanner (System.in);
        
        //Prompts user for number of hamburgers and stores it in an int
        System.out.println("How many hamburgers?: ");
        int numberOfHamburgers = keyboard.nextInt();
        //Prompts user for number of cheeseburgers and stores it in an int
        System.out.println("How many Cheeseburgers?: ");
        int numberOfCheeseburgers = keyboard.nextInt();
        //Prompts user for number of sodas and stores it in an int
        System.out.println("How many sodas?: ");
        int numberOfSodas = keyboard.nextInt();
        //Prompts user for number of fries and stores it in an int
        System.out.println("How many Fries?: ");
        int numberOfFries = keyboard.nextInt();
        //Consume remaining newline
        keyboard.nextLine();
        //Prompts user for name and stores it in a string
        System.out.println("What is your name?: ");
        String name = keyboard.nextLine();
        //Calculate total cost
        double total = ((numberOfHamburgers * 1.25) + (numberOfCheeseburgers * 1.50) + (numberOfSodas * 1.95) + (numberOfFries * .95));
        //Format and display output of name and cost
        System.out.printf(name.toUpperCase() + ", your total cost is $%,.2f",total);

    }
}