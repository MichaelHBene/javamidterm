package ch3n4;

public class Lawn {
	private double width;
	private double length;
	
	public void setWidth(double widthInput) {
		width = widthInput;
	}
	
	public double getWidth() {
		return width;
	}
	
	public void setLength(double lengthInput) {
		length = lengthInput;
	}
	
	public double getLength() {
		return length;
	}
	
	public Lawn() {
		
	}
	
	public Lawn(double widthInput, double lengthInput) {
		width = widthInput;
		length = lengthInput;
	}
	
	public double calcArea() {
		double squareFeet = width * length;
		return squareFeet;
	}
	
}
