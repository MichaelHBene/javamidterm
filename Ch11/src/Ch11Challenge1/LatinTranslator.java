package Ch11Challenge1;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.*;



public class LatinTranslator extends Application{
	
	private Label leftLabel;
	private Label rightLabel;
	private Label centerLabel;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) {
		Button button1 = new Button("Sinister");
		Button button2 = new Button("Dexter");
		Button button3 = new Button("Medium");
		button1.setOnAction(new ButtonClickHandler(1));
		button2.setOnAction(new ButtonClickHandler(2));
		button3.setOnAction(new ButtonClickHandler(3));
		HBox buttonHBox = new HBox(10, button1, button2, button3);
		leftLabel = new Label("Left");
		rightLabel = new Label("Right");
		centerLabel = new Label("Center");
		HBox labelHBox = new HBox(40, leftLabel, rightLabel, centerLabel);
		leftLabel.setVisible(false);
		rightLabel.setVisible(false);
		centerLabel.setVisible(false);
		VBox buttonLabelDisplay = new VBox(10, buttonHBox, labelHBox);
		Scene scene = new Scene(buttonLabelDisplay);
		buttonHBox.setAlignment(Pos.TOP_CENTER);
		labelHBox.setAlignment(Pos.CENTER);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Latin Translator");
		primaryStage.show();
	}
	
	class ButtonClickHandler implements EventHandler<ActionEvent> {
		private int buttonField;
		public ButtonClickHandler(int i) {
			// TODO Auto-generated constructor stub
			buttonField = i;
		}

		@Override
		public void handle(ActionEvent event) {

			if(buttonField == 1) {
				leftLabel.setVisible(true);
				rightLabel.setVisible(false);
				centerLabel.setVisible(false);
			}
			else if(buttonField == 2) {
				leftLabel.setVisible(false);
				rightLabel.setVisible(true);
				centerLabel.setVisible(false);
			}
			else if(buttonField == 3) {
				leftLabel.setVisible(false);
				rightLabel.setVisible(false);
				centerLabel.setVisible(true);
			}
		}
	}

}

