package Ch11Challenge5;


import java.util.Random;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class HeadsOrTails extends Application{

	private Image heads = new Image("file:src\\Ch11Challenge5\\Heads.png");
	private Image tails = new Image("file:src\\Ch11Challenge5\\Tails.png");
	
	private ImageView coin = new ImageView();
	private Button flip = new Button("Flip Coin");
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		VBox StageDisplay = new VBox(5, coin, flip);
		flip.setOnAction(new ButtonClickHandler());
		StageDisplay.setAlignment(Pos.BASELINE_CENTER);
		Scene scene = new Scene(StageDisplay, 500, 500);
		coin.setImage(heads);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Coin Flip");
		primaryStage.show();
	}
	
	class ButtonClickHandler implements EventHandler<ActionEvent> {
		private int buttonField;
		public ButtonClickHandler() {
			// TODO Auto-generated constructor stub
		}

		@Override
		public void handle(ActionEvent event) {
			Random rand = new Random();
			int randomNumber = rand.nextInt(2);
			
			if(randomNumber == 0) {
				coin.setImage(heads);
			}
			else if(randomNumber == 1) {
				coin.setImage(tails);
			}
			
		}
	}

}
