package Ch11Challenge8;

import java.util.Random;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class JoesAutomotive extends Application{

	private Label oilLabel = new Label("Oil Change: $35");
	private Label lubeLabel = new Label("Lube Job: $25");
	private Label radiatorLabel = new Label("Radiator Flush: $50");
	private Label transmissionLabel = new Label("Transmision Flush: $120");
	private Label inspectionLabel = new Label("Inspection: $35");
	private Label mufflerLabel = new Label("Muffler Replacement: $200");
	private Label tireLabel = new Label("Tire Rotation: $20");
	private Label nonRouteLabel = new Label("Nonroutine Service: $60/hr");
	private Label hoursLabel = new Label("If Selecting Nonroutine Service, please enter hours:");
	private CheckBox oilChange = new CheckBox();
	private CheckBox lubeJob = new CheckBox();
	private CheckBox radiatorFlush = new CheckBox();
	private CheckBox transmissionFlush = new CheckBox();
	private CheckBox inspectionCheck = new CheckBox();
	private CheckBox mufflerReplacement = new CheckBox();
	private CheckBox tireRotation = new CheckBox();
	private CheckBox nonRoutine = new CheckBox();
	private TextField hoursInput = new TextField();
	private Button calculateCostBtn = new Button("Calculate Cost");
	private Label totalCost = new Label();
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		VBox oil = new VBox(5, oilLabel, oilChange);
		VBox lube = new VBox(5, lubeLabel, lubeJob);
		VBox radiator = new VBox(5, radiatorLabel, radiatorFlush);
		VBox transmission = new VBox(5, transmissionLabel, transmissionFlush);
		VBox inspection = new VBox(5, inspectionLabel, inspectionCheck);
		VBox muffler = new VBox(5, mufflerLabel, mufflerReplacement);
		VBox tire = new VBox(5, tireLabel, tireRotation);
		VBox service = new VBox(5, nonRouteLabel, nonRoutine);
		HBox hours = new HBox(5, hoursLabel, hoursInput);
		hours.setAlignment(Pos.BASELINE_CENTER);
		calculateCostBtn.setOnAction(new ButtonClickHandler());
		HBox checkboxDisplays = new HBox(10, oil, lube, radiator, transmission, inspection, muffler, tire, service);
		checkboxDisplays.setAlignment(Pos.BASELINE_CENTER);
		VBox StageDisplay = new VBox(25, checkboxDisplays, hours, calculateCostBtn, totalCost);
		StageDisplay.setAlignment(Pos.BASELINE_CENTER);
		Scene scene = new Scene(StageDisplay, 1000, 200);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Joe's Automotive");
		primaryStage.show();
	}
	
	class ButtonClickHandler implements EventHandler<ActionEvent> {
		public ButtonClickHandler() {
			// TODO Auto-generated constructor stub
		}

		@Override
		public void handle(ActionEvent event) {
			int total = 0;
			
			if(oilChange.isSelected() == true) {
				total += 35;
			}
			if(lubeJob.isSelected() == true) {
				total += 25;
			}
			if(radiatorFlush.isSelected() == true) {
				total += 50;
			}
			if(transmissionFlush.isSelected() == true) {
				total += 120;
			}
			if(inspectionCheck.isSelected() == true) {
				total += 35;
			}
			if(mufflerReplacement.isSelected() == true) {
				total += 200;
			}
			if(tireRotation.isSelected() == true) {
				total += 20;
			}
			if(nonRoutine.isSelected() == true) {
				int serviceCost = (int)Double.parseDouble(hoursInput.getText()) * 60;
				total += serviceCost;
			}
			
			totalCost.setText("Total Cost: $" + total);
		}
	}

}
