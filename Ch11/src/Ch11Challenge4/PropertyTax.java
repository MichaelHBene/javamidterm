package Ch11Challenge4;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class PropertyTax extends Application{

	private Label propertyPrompt = new Label("Enter Property Value:");
	private TextField propertyValue = new TextField();
	private Button calcCosts = new Button("Calculate Assess Value amd Property Tax");
	private Label assessmentLabel = new Label("");
	private Label propertyLabel = new Label("");
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub	
		HBox propertyInput = new HBox(5, propertyPrompt, propertyValue);
		propertyInput.setAlignment(Pos.BASELINE_CENTER);
		calcCosts.setOnAction(new ButtonClickHandler());
		VBox StageDisplay = new VBox(5, propertyInput, calcCosts, assessmentLabel, propertyLabel);
		StageDisplay.setAlignment(Pos.BASELINE_CENTER);
		Scene scene = new Scene(StageDisplay, 325, 150);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Property Tax");
		primaryStage.show();
		
	}
	
	
	class ButtonClickHandler implements EventHandler<ActionEvent> {
		private int buttonField;
		public ButtonClickHandler() {
			// TODO Auto-generated constructor stub
		}

		@Override
		public void handle(ActionEvent event) {
			double propertyInput = Double.parseDouble(propertyValue.getText());
			double assessment = propertyInput * .6;
			int TAX_CALC = 100;
			int numTaxApplied = (int) (assessment / TAX_CALC);
			System.out.println(numTaxApplied);
			double tax = .64 * numTaxApplied;
			
			assessmentLabel.setText("Assessed Value: $" + assessment);
			propertyLabel.setText("Property Tax: $" + tax);
			
		}
	}

}
