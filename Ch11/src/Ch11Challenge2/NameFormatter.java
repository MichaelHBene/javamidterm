package Ch11Challenge2;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class NameFormatter extends Application{

	private Label fnPrompt = new Label("Enter First Name:");
	private Label mnPrompt = new Label("Enter Middle Name:");
	private Label lnPrompt = new Label("Enter Last Name:");
	private Label ptPrompt = new Label("Enter Preferred Title:");
	
	private TextField fnField = new TextField();
	private TextField mnField = new TextField();
	private TextField lnField = new TextField();
	private TextField ptField = new TextField();
	
	private Button opt1Btn = new Button("T/F/M/L");
	private Button opt2Btn = new Button("F/M/L");
	private Button opt3Btn = new Button("F/L");
	private Button opt4Btn = new Button("L/F/M/T");
	private Button opt5Btn = new Button("L/F/M");
	private Button opt6Btn = new Button("L/F");
	
	private Label nameDisplay = new Label("");
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		HBox writeFirstName = new HBox(5,fnPrompt, fnField);
		HBox writeMiddleName = new HBox(5, mnPrompt, mnField);
		HBox writeLastName = new HBox(5, lnPrompt, lnField);
		HBox writePreferredTitle = new HBox(5, ptPrompt, ptField);
		HBox formatButtons = new HBox(5, opt1Btn, opt2Btn, opt3Btn, opt4Btn, opt5Btn, opt6Btn);
		opt1Btn.setOnAction(new ButtonClickHandler(1));
		opt2Btn.setOnAction(new ButtonClickHandler(2));
		opt3Btn.setOnAction(new ButtonClickHandler(3));
		opt4Btn.setOnAction(new ButtonClickHandler(4));
		opt5Btn.setOnAction(new ButtonClickHandler(5));
		opt6Btn.setOnAction(new ButtonClickHandler(6));
		VBox StageDisplay = new VBox(5, writeFirstName, writeMiddleName, writeLastName, writePreferredTitle, formatButtons, nameDisplay);
		StageDisplay.setAlignment(Pos.BASELINE_CENTER);
		Scene scene = new Scene(StageDisplay, 325, 250);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Name Formatter");
		primaryStage.show();
	}
	
	class ButtonClickHandler implements EventHandler<ActionEvent> {
		private int buttonField;
		public ButtonClickHandler(int i) {
			// TODO Auto-generated constructor stub
			buttonField = i;
		}

		@Override
		public void handle(ActionEvent event) {

			if(buttonField == 1) {
				nameDisplay.setText(ptField.getText() + " " + fnField.getText() + " " + mnField.getText() + " " + lnField.getText());
			}
			else if(buttonField == 2) {
				nameDisplay.setText(fnField.getText() + " " + mnField.getText() + " " + lnField.getText());
			}
			else if(buttonField == 3) {
				nameDisplay.setText(fnField.getText() + " " + lnField.getText());
			}
			else if(buttonField == 4) {
				nameDisplay.setText(lnField.getText() + ", " + fnField.getText() + " " + mnField.getText() + ", " + ptField.getText());
			}
			else if(buttonField == 5) {
				nameDisplay.setText(lnField.getText() + ", " + fnField.getText() + " " + mnField.getText());
			}
			else if(buttonField == 6) {
				nameDisplay.setText(lnField.getText() + ", " + fnField.getText());
			}
		}
	}


}
