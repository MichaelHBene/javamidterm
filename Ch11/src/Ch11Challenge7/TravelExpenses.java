package Ch11Challenge7;

import java.util.Random;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import java.lang.*;

public class TravelExpenses extends Application{

	private Label daysLabel = new Label("Enter days on trip:");
	private TextField daysInput = new TextField();
	private Label airLabel = new Label("Enter airfare cost:");
	private TextField airInput = new TextField();
	private Label rentalLabel = new Label("Enter car rental fees:");
	private TextField rentalInput = new TextField();
	private Label milesLabel = new Label("Enter miles driven");
	private TextField milesInput = new TextField();
	private Label parkingLabel = new Label("Enter parking fees:");
	private TextField parkingInput = new TextField();
	private Label taxiLabel = new Label("Enter taxi charges:");
	private TextField taxiInput = new TextField();
	private Label conferenceLabel = new Label("Enter conference or seminar registration fees:");
	private TextField conferenceInput = new TextField();
	private Label lodgingLabel = new Label("Enter lodging charges per night:");
	private TextField lodgingInput = new TextField();
	private Label totalExpenses = new Label();
	private Label totalAllowed = new Label();
	private Label amountOwed = new Label();
	private Label amountSaved = new Label();
	private Button calcBtn = new Button("Calculate Costs");
	
	private double COMPANY_MEALS = 47;
	private double COMPANY_PARKING = 20;
	private double COMPANY_TAXI = 40;
	private double COMPANY_LODGING = 195;
	private double COMPANY_MILEAGE = .42;
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		launch(args);
		
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		
		HBox daysDisplay = new HBox(5, daysLabel, daysInput);
		daysDisplay.setAlignment(Pos.BASELINE_CENTER);
		HBox airDisplay = new HBox(5, airLabel, airInput);
		airDisplay.setAlignment(Pos.BASELINE_CENTER);
		HBox rentalDisplay = new HBox(5, rentalLabel, rentalInput);
		rentalDisplay.setAlignment(Pos.BASELINE_CENTER);
		HBox milesDisplay = new HBox(5, milesLabel, milesInput);
		milesDisplay.setAlignment(Pos.BASELINE_CENTER);
		HBox parkingDisplay = new HBox(5, parkingLabel, parkingInput);
		parkingDisplay.setAlignment(Pos.BASELINE_CENTER);
		HBox taxiDisplay = new HBox(5, taxiLabel, taxiInput);
		taxiDisplay.setAlignment(Pos.BASELINE_CENTER);
		HBox conferenceDisplay = new HBox(5, conferenceLabel, conferenceInput);
		conferenceDisplay.setAlignment(Pos.BASELINE_CENTER);
		HBox lodgingDisplay = new HBox(5, lodgingLabel, lodgingInput);
		lodgingDisplay.setAlignment(Pos.BASELINE_CENTER);
		
		VBox StageDisplay = new VBox(10, daysDisplay, airDisplay, rentalDisplay, milesDisplay, parkingDisplay, taxiDisplay, conferenceDisplay, lodgingDisplay, calcBtn, totalExpenses, totalAllowed, amountOwed, amountSaved);
		calcBtn.setOnAction(new ButtonClickHandler());
		StageDisplay.setAlignment(Pos.BASELINE_CENTER);
		Scene scene = new Scene(StageDisplay, 500, 500);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Travel Expenses");
		primaryStage.show();
		
	}
	
	class ButtonClickHandler implements EventHandler<ActionEvent> {
		private int buttonField;
		public ButtonClickHandler() {
			// TODO Auto-generated constructor stub
		}

		@Override
		public void handle(ActionEvent event) {
			double days = Double.parseDouble(daysInput.getText());
			double airFees = Double.parseDouble(airInput.getText());
			double rentalFees = Double.parseDouble(rentalInput.getText());
			double miles = Double.parseDouble(milesInput.getText());
			double parkingFees = Double.parseDouble(parkingInput.getText());
			double taxiFees = Double.parseDouble(parkingInput.getText());
			double conferenceFees = Double.parseDouble(conferenceInput.getText());
			double lodgingFees = (Double.parseDouble(lodgingInput.getText()) * days);
			
			double companyParking = COMPANY_PARKING * days;
			double companyTaxi = COMPANY_TAXI * days;
			double companyLodging = COMPANY_LODGING * days;
			
			double mealTotal = days * COMPANY_MEALS;
			double parkingTotal = parkingFees - companyParking;
			double taxiTotal = taxiFees - companyTaxi;
			double lodgingTotal = lodgingFees - companyLodging;
			double milesTotal = miles * COMPANY_MILEAGE;
			
			
			
			double expenseTotal = (mealTotal + rentalFees + milesTotal + parkingFees + taxiFees + conferenceFees + lodgingFees);
			double owedTotal = (airFees + rentalFees + conferenceFees);
			
			if(parkingTotal < 0) {
				parkingTotal *= -1;
			}
			else {
				owedTotal += parkingTotal;
			}
			if(taxiTotal < 0) {
				taxiTotal *= -1;
			}
			else {
				owedTotal += taxiTotal;
			}
			if(lodgingTotal < 0) {
				lodgingTotal *= -1;
			}
			else {
				owedTotal += lodgingTotal;
			}
			
			double allowedTotal = (companyParking + companyTaxi + companyLodging + mealTotal + milesTotal);
			
			double savedTotal = (mealTotal + parkingTotal + taxiTotal + lodgingTotal + milesTotal);
			
			totalExpenses.setText("Total Expense: $" + expenseTotal);
			totalAllowed.setText("Total Allowed: $" + allowedTotal);
			amountOwed.setText("Total Owed: $" + owedTotal);
			amountSaved.setText("Total Saved: $" + savedTotal);
			
			
		}
	}

}
