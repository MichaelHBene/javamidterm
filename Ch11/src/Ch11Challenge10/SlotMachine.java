package Ch11Challenge10;

import java.util.Random;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;

public class SlotMachine extends Application{

	private double totalWon = 0;
	private double winnings = 0;
	
	private Image cherry = new Image("file:src\\Ch11Challenge10\\cherry.png");
	private Image apple = new Image("file:src\\Ch11Challenge10\\apple.png");
	private Image grape = new Image("file:src\\Ch11Challenge10\\grape.png");
	private Image bar = new Image("file:src\\Ch11Challenge10\\bar.png");
	
	private ImageView slot1 = new ImageView();
	private ImageView slot2 = new ImageView();
	private ImageView slot3 = new ImageView();
	
	private Label betLabel = new Label("Bet: $");
	private TextField betInput = new TextField();
	private Label amountSpin = new Label("Amount won this spin: $" + winnings);
	private Label amountTotal = new Label("Total amount won: $" + totalWon);
	
	private Button spinBtn = new Button("Spin");
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		slot1.setImage(bar);
		slot2.setImage(bar);
		slot3.setImage(bar);
		spinBtn.setOnAction(new ButtonClickHandler());
		HBox SlotDisplay = new HBox(5, slot1, slot2, slot3);
		HBox BetDisplay = new HBox(5, betLabel, betInput);
		VBox AmountsDisplay = new VBox(5, amountSpin, amountTotal);
		HBox InfoDisplay = new HBox(50,BetDisplay, AmountsDisplay);
		VBox StageDisplay = new VBox(5, SlotDisplay, InfoDisplay, spinBtn);
		StageDisplay.setAlignment(Pos.BASELINE_CENTER);
		Scene scene = new Scene(StageDisplay, 450, 250);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Slot Machine");
		primaryStage.show();
	}
	
	class ButtonClickHandler implements EventHandler<ActionEvent> {
		private int buttonField;
		public ButtonClickHandler() {
			// TODO Auto-generated constructor stub
		}

		@Override
		public void handle(ActionEvent event) {
			winnings = 0;
			Random rand = new Random();
			int randomNumber1 = rand.nextInt(4);
			int randomNumber2 = rand.nextInt(4);
			int randomNumber3 = rand.nextInt(4);
			
			if(randomNumber1 == 0) {
				slot1.setImage(apple);
			}else if(randomNumber1 == 1) {
				slot1.setImage(bar);
			}else if(randomNumber1 == 2) {
				slot1.setImage(cherry);
			}
			else if(randomNumber1 == 3) {
				slot1.setImage(grape);
			}
			
			if(randomNumber2 == 0) {
				slot2.setImage(apple);
			}else if(randomNumber2 == 1) {
				slot2.setImage(bar);
			}else if(randomNumber2 == 2) {
				slot2.setImage(cherry);
			}
			else if(randomNumber2 == 3) {
				slot2.setImage(grape);
			}
			
			if(randomNumber3 == 0) {
				slot3.setImage(apple);
			}else if(randomNumber3 == 1) {
				slot3.setImage(bar);
			}else if(randomNumber3 == 2) {
				slot3.setImage(cherry);
			}
			else if(randomNumber3 == 3) {
				slot3.setImage(grape);
			}
			
			double bet = Double.parseDouble(betInput.getText());
			
			if(randomNumber1 == randomNumber2 && randomNumber1 == randomNumber3) {
				winnings = bet * 3;
			}else if((randomNumber1 == randomNumber2 && randomNumber2 != randomNumber3) || (randomNumber1 == randomNumber3 && randomNumber2 != randomNumber3)) {
				winnings = bet * 2;
			}else if((randomNumber2 == randomNumber3 && randomNumber2 != randomNumber1) || (randomNumber1 == randomNumber2 && randomNumber2 != randomNumber3)) {
				winnings = bet * 2;
			}
			
			totalWon += winnings;
			amountSpin.setText("Amount won this spin: $" + winnings);
			amountTotal.setText("Total amount won: $" + totalWon);
			
			
		}
	}
	
}
