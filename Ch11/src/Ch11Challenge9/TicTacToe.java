package Ch11Challenge9;

import java.util.Random;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class TicTacToe extends Application{

	private ImageView[][] ticTacGrid = new ImageView[3][3];

	private Image xImage = new Image("file:src\\Ch11Challenge9\\x.png");
	private Image oImage = new Image("file:src\\Ch11Challenge9\\o.png");
	
	private ImageView grid1 = new ImageView();
	private ImageView grid2 = new ImageView();
	private ImageView grid3 = new ImageView();
	private ImageView grid4 = new ImageView();
	private ImageView grid5 = new ImageView();
	private ImageView grid6 = new ImageView();
	private ImageView grid7 = new ImageView();
	private ImageView grid8 = new ImageView();
	private ImageView grid9 = new ImageView();
	
	private Button playBtn = new Button("Play Game");
	
	private Label winLabel = new Label();
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		ticTacGrid[0][0] = grid1;
		ticTacGrid[0][1] = grid2;
		ticTacGrid[0][2] = grid3;
		ticTacGrid[1][0] = grid4;
		ticTacGrid[1][1] = grid5;
		ticTacGrid[1][2] = grid6;
		ticTacGrid[2][0] = grid7;
		ticTacGrid[2][1] = grid8;
		ticTacGrid[2][2] = grid9;
		playBtn.setOnAction(new ButtonClickHandler());
		HBox row1 = new HBox(10, grid1, grid2, grid3);
		HBox row2 = new HBox(10, grid4, grid5, grid6);
		HBox row3 = new HBox(10, grid7, grid8, grid9);
		row1.setAlignment(Pos.BASELINE_CENTER);
		row2.setAlignment(Pos.BASELINE_CENTER);
		row3.setAlignment(Pos.BASELINE_CENTER);
		VBox grid = new VBox(10, row1, row2, row3, playBtn, winLabel);
		grid.setAlignment(Pos.BASELINE_CENTER);
		Scene scene = new Scene(grid, 1000, 1000);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Tic-Tac-Toe");
		primaryStage.show();
	}
	
	class ButtonClickHandler implements EventHandler<ActionEvent> {
		public ButtonClickHandler() {
			// TODO Auto-generated constructor stub
		}

		@Override
		public void handle(ActionEvent event) {
			Random rand = new Random();
			int randomNumber1 = rand.nextInt(2);
			int randomNumber2 = rand.nextInt(2);
			int randomNumber3 = rand.nextInt(2);
			int randomNumber4 = rand.nextInt(2);
			int randomNumber5 = rand.nextInt(2);
			int randomNumber6 = rand.nextInt(2);
			int randomNumber7 = rand.nextInt(2);
			int randomNumber8 = rand.nextInt(2);
			int randomNumber9 = rand.nextInt(2);
			if(randomNumber1 == 0) {
				ticTacGrid[0][0].setImage(oImage);
			}
			else if(randomNumber1 == 1) {
				ticTacGrid[0][0].setImage(xImage);
			}
			if(randomNumber2 == 0) {
				ticTacGrid[0][1].setImage(oImage);
			}
			else if(randomNumber2 == 1) {
				ticTacGrid[0][1].setImage(xImage);
			}
			if(randomNumber3 == 0) {
				ticTacGrid[0][2].setImage(oImage);
			}
			else if(randomNumber3 == 1) {
				ticTacGrid[0][2].setImage(xImage);
			}
			if(randomNumber4 == 0) {
				ticTacGrid[1][0].setImage(oImage);
			}
			else if(randomNumber4 == 1) {
				ticTacGrid[1][0].setImage(xImage);
			}
			if(randomNumber5 == 0) {
				ticTacGrid[1][1].setImage(oImage);
			}
			else if(randomNumber5 == 1) {
				ticTacGrid[1][1].setImage(xImage);
			}
			if(randomNumber6 == 0) {
				ticTacGrid[1][2].setImage(oImage);
			}
			else if(randomNumber6 == 1) {
				ticTacGrid[1][2].setImage(xImage);
			}
			if(randomNumber7 == 0) {
				ticTacGrid[2][0].setImage(oImage);
			}
			else if(randomNumber7 == 1) {
				ticTacGrid[2][0].setImage(xImage);
			}
			if(randomNumber8 == 0) {
				ticTacGrid[2][1].setImage(oImage);
			}
			else if(randomNumber8 == 1) {
				ticTacGrid[2][1].setImage(xImage);
			}
			if(randomNumber9 == 0) {
				ticTacGrid[2][2].setImage(oImage);
			}
			else if(randomNumber9 == 1) {
				ticTacGrid[2][2].setImage(xImage);
			}
			
			
			/*
			for(int x = 0; x < 3; ++x) {
				for(int i = 0; x < 3; ++i) {
					Random rand = new Random();
					int randomNumber = rand.nextInt(2);
					if(randomNumber == 0) {
						ticTacGrid[x][i].setImage(oImage);
					}
					else if(randomNumber == 1) {
						ticTacGrid[x][i].setImage(xImage);
					}
				}
			}
			*/
			
			if(ticTacGrid[0][0].getImage() == ticTacGrid[0][1].getImage() && ticTacGrid[0][1].getImage() == ticTacGrid[0][2].getImage()) {
				if(ticTacGrid[0][0].getImage() == xImage) {
					winLabel.setText("X Wins!");
				}else if(ticTacGrid[0][0].getImage() == oImage) {
					winLabel.setText("O Wins!");
				}
			}
			else if(ticTacGrid[1][0].getImage() == ticTacGrid[1][1].getImage() && ticTacGrid[1][1].getImage() == ticTacGrid[1][2].getImage()) {
				if(ticTacGrid[1][0].getImage() == xImage) {
					winLabel.setText("X Wins!");
				}else if(ticTacGrid[1][0].getImage() == oImage) {
					winLabel.setText("O Wins!");
				}
			}
			else if(ticTacGrid[2][0].getImage() == ticTacGrid[2][1].getImage() && ticTacGrid[2][1].getImage() == ticTacGrid[2][2].getImage()) {
				if(ticTacGrid[2][0].getImage() == xImage) {
					winLabel.setText("X Wins!");
				}else if(ticTacGrid[2][0].getImage() == oImage) {
					winLabel.setText("O Wins!");
				}
			}
			else if(ticTacGrid[0][0].getImage() == ticTacGrid[1][0].getImage() && ticTacGrid[1][0].getImage() == ticTacGrid[2][0].getImage()) {
				if(ticTacGrid[0][0].getImage() == xImage) {
					winLabel.setText("X Wins!");
				}else if(ticTacGrid[0][0].getImage() == oImage) {
					winLabel.setText("O Wins!");
				}
			}
			else if(ticTacGrid[0][1].getImage() == ticTacGrid[1][1].getImage() && ticTacGrid[1][1].getImage() == ticTacGrid[2][1].getImage()) {
				if(ticTacGrid[0][1].getImage() == xImage) {
					winLabel.setText("X Wins!");
				}else if(ticTacGrid[0][1].getImage() == oImage) {
					winLabel.setText("O Wins!");
				}
			}
			else if(ticTacGrid[0][2].getImage() == ticTacGrid[1][2].getImage() && ticTacGrid[1][2].getImage() == ticTacGrid[2][2].getImage()) {
				if(ticTacGrid[0][2].getImage() == xImage) {
					winLabel.setText("X Wins!");
				}else if(ticTacGrid[0][2].getImage() == oImage) {
					winLabel.setText("O Wins!");
				}
			}
			else if(ticTacGrid[0][0].getImage() == ticTacGrid[1][1].getImage() && ticTacGrid[1][1].getImage() == ticTacGrid[2][2].getImage()) {
				if(ticTacGrid[0][0].getImage() == xImage) {
					winLabel.setText("X Wins!");
				}else if(ticTacGrid[0][0].getImage() == oImage) {
					winLabel.setText("O Wins!");
				}
			}
			else if(ticTacGrid[2][0].getImage() == ticTacGrid[1][1].getImage() && ticTacGrid[1][1].getImage() == ticTacGrid[0][2].getImage()) {
				if(ticTacGrid[2][0].getImage() == xImage) {
					winLabel.setText("X Wins!");
				}else if(ticTacGrid[2][0].getImage() == oImage) {
					winLabel.setText("O Wins!");
				}
			}
			else {
				winLabel.setText("It's a Tie!");
			}
		}
	}

}
