package Ch11Challenge6;

import java.util.Random;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class DiceRoller extends Application{

	private Image die1 = new Image("file:src\\Ch11Challenge6\\die1.png");
	private Image die2 = new Image("file:src\\Ch11Challenge6\\die2.png");
	private Image die3 = new Image("file:src\\Ch11Challenge6\\die3.png");
	private Image die4 = new Image("file:src\\Ch11Challenge6\\die4.png");
	private Image die5 = new Image("file:src\\Ch11Challenge6\\die5.png");
	private Image die6 = new Image("file:src\\Ch11Challenge6\\die6.png");
	
	private ImageView firstDie = new ImageView();
	private ImageView secondDie = new ImageView();
	private Button roll = new Button("Roll the Dice!");
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub
		HBox diceDisplay = new HBox(5, firstDie, secondDie);
		diceDisplay.setAlignment(Pos.BASELINE_CENTER);
		VBox StageDisplay = new VBox(5, diceDisplay, roll);
		roll.setOnAction(new ButtonClickHandler());
		StageDisplay.setAlignment(Pos.BASELINE_CENTER);
		Scene scene = new Scene(StageDisplay, 170, 150);
		firstDie.setImage(die1);
		secondDie.setImage(die2);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Dice Roll");
		primaryStage.show();
	}
	
	class ButtonClickHandler implements EventHandler<ActionEvent> {
		private int buttonField;
		public ButtonClickHandler() {
			// TODO Auto-generated constructor stub
		}

		@Override
		public void handle(ActionEvent event) {
			Random rand = new Random();
			int randomNumber = rand.nextInt(6);
			int randomNumber2 = rand.nextInt(6);
			if(randomNumber == 0) {
				firstDie.setImage(die1);
			}
			else if(randomNumber == 1) {
				firstDie.setImage(die2);
			}
			else if(randomNumber == 2) {
				firstDie.setImage(die3);
			}
			else if(randomNumber == 3) {
				firstDie.setImage(die4);
			}
			else if(randomNumber == 4) {
				firstDie.setImage(die5);
			}
			else if(randomNumber == 5) {
				firstDie.setImage(die6);
			}
			
			if(randomNumber2 == 0) {
				secondDie.setImage(die1);
			}
			else if(randomNumber2 == 1) {
				secondDie.setImage(die2);
			}
			else if(randomNumber2 == 2) {
				secondDie.setImage(die3);
			}
			else if(randomNumber2 == 3) {
				secondDie.setImage(die4);
			}
			else if(randomNumber2 == 4) {
				secondDie.setImage(die5);
			}
			else if(randomNumber2 == 5) {
				secondDie.setImage(die6);
			}
			
		}
	}

}
