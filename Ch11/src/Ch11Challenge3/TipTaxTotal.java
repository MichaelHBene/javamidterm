package Ch11Challenge3;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class TipTaxTotal extends Application{
	
	private Label mealPrompt = new Label("Enter Meal Cost:");
	private TextField mealCost = new TextField();
	private Button calcCosts = new Button("Calculate Tip, Tax, and Total");
	private Label tipLabel = new Label("");
	private Label taxLabel = new Label("");
	private Label totalLabel = new Label("");
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// TODO Auto-generated method stub	
		HBox mealInput = new HBox(5, mealPrompt, mealCost);
		mealInput.setAlignment(Pos.BASELINE_CENTER);
		calcCosts.setOnAction(new ButtonClickHandler());
		VBox StageDisplay = new VBox(5, mealInput, calcCosts, tipLabel, taxLabel, totalLabel);
		StageDisplay.setAlignment(Pos.BASELINE_CENTER);
		Scene scene = new Scene(StageDisplay, 325, 150);
		primaryStage.setScene(scene);
		primaryStage.setTitle("Tip, Tax, and Total");
		primaryStage.show();
		
	}
	
	class ButtonClickHandler implements EventHandler<ActionEvent> {
		private int buttonField;
		public ButtonClickHandler() {
			// TODO Auto-generated constructor stub
		}

		@Override
		public void handle(ActionEvent event) {
			double mealPrice = Double.parseDouble(mealCost.getText());
			double tipPrice = mealPrice * .18;
			double taxPrice = mealPrice * .07;
			double totalPrice = mealPrice + tipPrice + taxPrice;
			
			tipLabel.setText("Tip Cost: $" + tipPrice);
			taxLabel.setText("Tax Cost: $" + taxPrice);
			totalLabel.setText("Total Cost: $" + totalPrice);
			
		}
	}

}
