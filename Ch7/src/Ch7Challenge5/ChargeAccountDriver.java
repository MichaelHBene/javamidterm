package Ch7Challenge5;

import java.io.FileNotFoundException;
import java.util.Scanner;
public class ChargeAccountDriver {

	public static void main(String[] args) throws FileNotFoundException {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		ChargeAccount account = new ChargeAccount();
		System.out.println("Please enter the account number:");
		boolean isValid = account.getChargeValid(keyboard.nextInt());
		
		if(isValid) {
			System.out.println("The account number is valid.");
		}
		else {
			System.out.println("The account number is not valid.");
		}
	}

}
