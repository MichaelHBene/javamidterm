package Ch7Challenge7;

import java.util.ArrayList;
import java.util.Scanner;
import java.io.*;

public class Magic8Ball {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		File inputFile = new File("src\\Ch7Challenge7\\8_ball_responses.txt");
		Scanner responses = new Scanner(inputFile);
		
		ArrayList<String> eightBallResponses = new ArrayList<String>();
		while(responses.hasNext()) {
			eightBallResponses.add(responses.nextLine());
		}
		
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Please enter your question:");
		
		int max = 9;
		int min = 0;
		int range = max - min + 1;		
		int rand = (int) (Math.random()* range) + min;
		
		System.out.println("\nYou asked: " + keyboard.nextLine() + "\nYour answer is: " + eightBallResponses.get(rand));
		
	}


}
