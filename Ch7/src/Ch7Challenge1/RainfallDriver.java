package Ch7Challenge1;

import java.util.Scanner;
public class RainfallDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		int count = 0;
		Rainfall raincatcher = new Rainfall();
		System.out.print("Enter a number for month " + (count + 1) + ":");
		double rainfall = keyboard.nextDouble();
		while (rainfall >= 0 && count <= 11)
		{
			raincatcher.setRainfall(rainfall, count);
		    count++;
		    if(count <= 11) {
			    System.out.print("Enter a number for month " + (count + 1) + ":");
				rainfall = keyboard.nextDouble();		
		    }	
		}
		
		if(rainfall < 0) {
			System.out.println("Error, rainfall cannot be negative.");
		}
		else {
			System.out.println("The total rainfall for the year is: " + raincatcher.getTotal());
			System.out.println("The average rainfall for this year is: " + raincatcher.getAvg());
			System.out.println("The highest recorded rainfall this year was in month " + raincatcher.getHighest());
			System.out.println("The lowest recorded rainfall this year was in month " + raincatcher.getLowest());	
		}
		
	}
}
