package Ch7Challenge8;

public class GradeBook {

	private String[] students = new String[5];
	private double[][] scores = new double[5][4];
	private double averageScore;
	
	/*
	public GradeBook(String[] s, double[][] d) {
		for(int x = 0; x < students.length; ++x) {
			students[x] = s[x];
		}
		for(int x = 0; x < scores.length; ++x) {
			for(int i = 0; i < 4; ++i) {
				scores[x][i] = d[x][i];
			}
		}
	}
	*/
	
	public GradeBook(String[] studentNames, double[][] testScores) {
		for(int x = 0; x < students.length; ++x) {
			students[x] = studentNames[x];
		}
		for(int x = 0; x < scores.length; ++x) {
			for(int i = 0; i < 4; ++i) {
				scores[x][i] = testScores[x][i];
			}
		}
	}

	public String getStudentName(int studentSub) {
		return students[studentSub];
	}
	
	public double getAvg(int studentSub) {
		double average = 0;
		
		for(int x = 0; x < 4; ++x) {
			average += scores[studentSub][x];
		}
		
		average /= 4;
		averageScore = average;
		return average;
	}
	
	public char getLetterGrade() {
		if(averageScore >= 90 && averageScore <= 100) {
			return 'A';
		}
		else if(averageScore >= 80 && averageScore <= 89) {
			return 'B';
		}
		else if(averageScore >= 70 && averageScore <= 79) {
			return 'C';
		}
		else if(averageScore >= 60 && averageScore <= 69) {
			return 'D';
		}
		else if(averageScore >= 0 && averageScore <= 59) {
			return 'F';
		}
		else {
			return 'U';
		}
	}
	
	public String toString() {
		String str = "";
		
		for (int x = 0; x < students.length; ++x) {
			str += (getStudentName(x) + ":\nAverage Score: " + getAvg(x) + "\nLetter Grade: " + getLetterGrade() + "\n");
		}
		
		return str;
		
	}
	
}
