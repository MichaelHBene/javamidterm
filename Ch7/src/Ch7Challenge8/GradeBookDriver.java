package Ch7Challenge8;

import java.util.Scanner;
public class GradeBookDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner keyboard = new Scanner(System.in);
		
		String[] studentNames = new String[5];
		double[][] testScores = new double[5][4];
		String studentName;
		double testScore;
		
		
		for (int x = 0; x < 5; ++x) {
			System.out.println("Please enter the name of student " + (x + 1));
			studentName = keyboard.nextLine();
			studentNames[x] = studentName;
			
			for(int i = 0; i < 4; ++i) {
				System.out.println("Please enter the score for test " + (i + 1));
				testScore = keyboard.nextDouble();
				while(testScore < 0 || testScore > 100) {
					System.out.println("Invalid Score. Please reenter score for test " + (i + 1));
					testScore = keyboard.nextDouble();
				}
				
				testScores[x][i] = testScore;
			}
			keyboard.nextLine();
		}
		
		GradeBook grades = new GradeBook(studentNames, testScores);
		
		System.out.println(grades.toString());

	}

}
