package Ch7Challenge6;

import java.util.Scanner;
public class DriversLicenseExam {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner keyboard = new Scanner(System.in);
		
		String answer;
		
		String[] answers = new String[20];
		
		
		for(int x = 0; x < 20; ++x) {
			System.out.println("Please enter A, B, C, or D for question " + (x + 1));
			answer = keyboard.nextLine();
			while(!answer.equalsIgnoreCase("a")&&!answer.equalsIgnoreCase("b")&&!answer.equalsIgnoreCase("c")&&!answer.equalsIgnoreCase("d")) {
				System.out.println("Invalid Answer! Please enter A, B, C, or D for question " + (x + 1));
				answer = keyboard.nextLine();
			}
			answers[x] = answer;
		}
		
		DriversLicense license = new DriversLicense(answers);
		
		System.out.println("You got " + license.totalCorrect() + " questions correct.");
		System.out.println("You got " + license.totalIncorrect() + " questions incorrect.");
		
		if(license.passed()) {
			System.out.println("Congratulations, you passed your exam!");
		}
		else {
			System.out.println("Unfortunately, you did not score high enough to pass your exam. Please schedule a retake at another date.");
		}
		System.out.println("You missed the following questions:");
		for(int x = 0; x < license.questionsMissed().size(); ++x) {
			System.out.println(license.questionsMissed().get(x));
		}
		
	}

}
