package Ch7Challenge6;

import java.util.ArrayList;
public class DriversLicense {
	private String[] answers = new String[20];
	private String[] correctAnswers = {"B","D","A","A","C","A","B","A","C","D","B","C","D","A","D","C","C","B","D","A"};
	private int numCorrect;
	
	public DriversLicense(String[] studentAnswers) {
		for(int x = 0; x < answers.length; ++x) {
			answers[x] = studentAnswers[x];
		}
	}
	
	public boolean passed() {
		boolean passed = false;
		if(numCorrect >= 15) {
			passed = true;
		}
		
		return passed;
	}
	
	public int totalCorrect() {
		int numCorrectAnswers = 0;
		for(int x = 0; x  < correctAnswers.length; ++x) {
			if(answers[x].equalsIgnoreCase(correctAnswers[x])) {
				++numCorrectAnswers;
			}
		}
		numCorrect = numCorrectAnswers;
		return numCorrectAnswers;
	}
	
	public int totalIncorrect() {
		int incorrectAnswers = 0;
		for(int x = 0; x  < correctAnswers.length; ++x) {
			if(!answers[x].equalsIgnoreCase(correctAnswers[x])) {
				++incorrectAnswers;
			}
		}
		return incorrectAnswers;
	}
	
	public ArrayList<Integer> questionsMissed() {
		ArrayList<Integer> questionsMissed = new ArrayList<Integer>(20);
		for(int x = 0; x  < correctAnswers.length; ++x) {
			if(!answers[x].equalsIgnoreCase(correctAnswers[x])) {
				questionsMissed.add(x + 1);
			}
		}
		return questionsMissed;
		
	}
	
}
