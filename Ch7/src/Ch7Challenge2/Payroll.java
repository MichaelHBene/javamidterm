package Ch7Challenge2;

public class Payroll {
	private int[] employeeId = {5658845, 4520125, 7895122, 8777541, 8451277, 1302850, 7580489};
	private int[] hours = new int[7];
	private double[] payRate = new double[7];
	private double[] wages = new double[7];
	
	
	public int getId(int subscript) {
		return employeeId[subscript];
	}
	
	public void setHours(int[] hoursInput) {
		for(int x = 0; x < hours.length; ++x) {
			hours[x] = hoursInput[x];
		}
		
	}
	
	public void setPayRate(double[] rateInput) {
		for(int x = 0; x < payRate.length; ++x) {
			payRate[x] = rateInput[x];
		}
	}
	
	public void setWages() {
		for(int x = 0; x < wages.length; ++x) {
			double wageToAdd = (double)payRate[x] * hours[x];
			wages[x] = wageToAdd;
		}
	}
	
	public String getWages(int sub) {		
		return ("Employee # " + employeeId[sub] + ": \nHours Worked: " + hours[sub] + "\nPay Rate: " + payRate[sub] + "\nGross Wages: $" + wages[sub]);
	}
	
}
