package Ch7Challenge3;

import java.util.Scanner;
public class ChargeAccountDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		ChargeAccount account = new ChargeAccount();
		System.out.println("Please enter the account number:");
		boolean isValid = account.getChargeValid(keyboard.nextInt());
		
		if(isValid) {
			System.out.println("The account number is valid.");
		}
		else {
			System.out.println("The account number is not valid.");
		}
	}

}
