package HOT;

import java.io.*;
import java.util.Scanner;
public class Sales {

	public static void main(String[] args)throws IOException {
		// TODO Auto-generated method stub

		double totalSales = 0;
		
		PrintWriter outputFile = new PrintWriter("src\\HOT\\WeeklySales.txt");
		
		Scanner keyboard = new Scanner(System.in);
		
		for (int x = 1; x <= 5; ++x) {
			System.out.println("Please enter the sales for day " + x + ":");
			
			double enteredSales = keyboard.nextDouble();
			
			while(enteredSales < 0) {
				System.out.println("Error. cannot enter negative number for sales. Please enter sales for day " + x + " again:");
				enteredSales = keyboard.nextDouble();
				
			}
			totalSales += enteredSales;
				
			System.out.printf("Sales for day %d" + ": " + "$%.2f \n", x, enteredSales);
			outputFile.printf("Sales for day %d" + ": " + "$%.2f", x, enteredSales);	
			outputFile.println("");
			System.out.println("Your sale has been appended to the file.");	
			
			
		}
		
		System.out.println("Your total sales for the 5-day period is: " + totalSales);
		outputFile.printf("Total Sales: $%.2f", totalSales);
		
		outputFile.close();
		
		
	}

}
