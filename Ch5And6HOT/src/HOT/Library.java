package HOT;

public class Library {

	private Book book1;
	private String name;
	
	public Library(Book bookInput, String nameInput) {
		book1 = bookInput;
		//bookInput.setAuthor("jaja");
		name = nameInput;
	}
	
	public String toString() {
		String str = ("Welcome to " + this.name + "\nWe have the following book(s) in our collection: \n" + book1.getName() + ": \n Publish Date: " + book1.getDate() + "\n Author: " + book1.getAuthor());
		return str;
	}
}
