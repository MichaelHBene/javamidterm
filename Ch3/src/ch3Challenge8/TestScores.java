package ch3Challenge8;

public class TestScores {
	private double test1;
	private double test2;
	private double test3;
	
	public void setTest1(double score1) {
		test1 = score1;
	}
	public double getTest1() {
		return test1;
	}
	public void setTest2(double score2) {
		test2 = score2;
	}
	public double getTest2() {
		return test2;
	}
	public void setTest3(double score3) {
		test3 = score3;
	}
	public double getTest3() {
		return test3;
	}
	
	public double getTestAverage() {
		double averageScore = ((test1 + test2 + test3) / 3);
		return averageScore;
	}
}
