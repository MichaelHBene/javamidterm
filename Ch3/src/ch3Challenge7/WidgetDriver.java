package ch3Challenge7;

public class WidgetDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Widget widgetOrder = new Widget();
		
		widgetOrder.setWidgetsToProduce(1000);
		System.out.println("Your order of " + widgetOrder.getWidgetsToProduce() + " widgets will be completed in " + widgetOrder.getDaysToProduce() + " days.");
	}

}
