package ch3Challenge7;

public class Widget {
	private int widgetsRequested;
	private int shiftsPerDay = 2;
	private int hoursPerShift = 8;
	private int widgetsPerHour = 10;
	private int widgetsPerDay = widgetsPerHour * (shiftsPerDay * hoursPerShift);
	public void setWidgetsToProduce(int numWidgets) {
		widgetsRequested = numWidgets;
	}
	
	public int getWidgetsToProduce() {
		return widgetsRequested;
	}
	
	public int getDaysToProduce() {
		int daysToComplete = widgetsRequested / widgetsPerDay;
		return daysToComplete;
	}
}
