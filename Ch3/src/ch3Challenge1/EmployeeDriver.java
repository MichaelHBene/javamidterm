package ch3Challenge1;
public class EmployeeDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Employee emp1 = new Employee();
		Employee emp2 = new Employee();
		Employee emp3 = new Employee();
		
		//Create Susan Meyers
		emp1.setName("Susan Meyers");
		emp1.setIdNumber(47899);
		emp1.setDepartment("Accounting");
		emp1.setPosition("Vice President");
		//Create Mark Jones
		emp2.setName("Mark Jones");
		emp2.setIdNumber(39119);
		emp2.setDepartment("IT");
		emp2.setPosition("Programmer");
		//Create Joy Rogers
		emp3.setName("Joy Rogers");
		emp3.setIdNumber(81774);
		emp3.setDepartment("Manufacturing");
		emp3.setPosition("Engineer");
		
		System.out.println("Employee 1: " + emp1.getName() + " ID#: " + emp1.getIdNumber() + " " + emp1.getDepartment() + ", " + emp1.getPosition());
		
		System.out.println("Employee 2: " + emp2.getName() + " ID#: " + emp2.getIdNumber() + " " + emp2.getDepartment() + ", " + emp2.getPosition());
		
		System.out.println("Employee : " + emp3.getName() + " ID#: " + emp3.getIdNumber() + " " + emp3.getDepartment() + ", " + emp3.getPosition());
	
		
		
	}

}
