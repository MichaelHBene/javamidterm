package ch3Challenge4;

public class Temperature {
	private double ftemp;
	
	public void setFahrenheit(double fahrenheit) {
		ftemp = fahrenheit;
	}
	
	public double getFahrenheit() {
		return ftemp;
	}
	
	public double getCelsius() {
		double ctemp;
		ctemp = (((double)5/9)*(ftemp - 32));
		return ctemp;
	}
	
	public double getKelvin() {
		double ktemp;
		ktemp = (((double)5/9)*(ftemp - 32)) + 273;
		return ktemp;
	}
}
