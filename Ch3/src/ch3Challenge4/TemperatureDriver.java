package ch3Challenge4;

import java.util.Scanner;
public class TemperatureDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner (System.in);
		System.out.println("What temperature is it in Fahrenheit?:");
		double fahrenheit = keyboard.nextDouble();
		
		Temperature temp1 = new Temperature();

		temp1.setFahrenheit(fahrenheit);
		
		System.out.println("The temperature in Celsius is: " + temp1.getCelsius() + " degrees");
		System.out.println("The temperature in Kelvin is: " + temp1.getKelvin() + " degrees");
		
	}

}
