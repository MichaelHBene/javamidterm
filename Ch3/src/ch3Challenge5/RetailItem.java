package ch3Challenge5;

public class RetailItem {
	private String description;
	private int unitsOnHand;
	private double price;
	
	public void setDescription(String item) {
		description = item;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setUnits(int numUnits) {
		unitsOnHand = numUnits;
	}
	
	public int getUnits() {
		return unitsOnHand;
	}
	
	public void setPrice(double cost) {
		price = cost;
	}
	
	public double getPrice() {
		return price;
	}
	
	
}
