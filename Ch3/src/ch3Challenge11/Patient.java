package ch3Challenge11;

public class Patient {
	private String first;
	private String middle;
	private String last;
	private String street;
	private String city;
	private String state;
	private String zip;
	private String phone;
	private String eName;
	private String ePhone;
	
	public Patient(String firstName, String middleName, String lastName, String streetName, String cityName, String stateName, String zipCode, String phoneNumber, String emergencyName, String emergencyPhone) {
		first = firstName;
		middle = middleName;
		last = lastName;
		street = streetName;
		city = cityName;
		state = stateName;
		zip = zipCode;
		phone = phoneNumber;
		eName = emergencyName;
		ePhone = emergencyPhone;
	}
	public void setFName (String firstName) {
		first = firstName;
	}
	public void setMName(String middleName) {
		middle = middleName;
	}
	public void setLName(String lastName) {
		last = lastName;
	}
	public void setStreet(String streetName) {
		street = streetName;
	}
	public void setCity(String cityName) {
		city = cityName;
	}
	public void setState(String stateName) {
		state = stateName;
	}
	public void setZip(String zipCode) {
		zip = zipCode;
	}
	public void setPhone(String phoneNumber) {
		phone = phoneNumber;
	}
	public void setEName(String emergencyName) {
		eName = emergencyName;
	}
	public void setEPhone(String emergencyPhone) {
		ePhone = emergencyPhone;
	}
	public String setFName() {
		return first;
	}
	public String getMName() {
		return middle;
	}
	public String getLName() {
		return last;
	}
	public String getStreet() {
		return street;
	}
	public String getCity() {
		return city;
	}
	public String getState() {
		return state;
	}
	public String getZip() {
		return zip;
	}
	public String getPhone() {
		return phone;
	}
	public String getEName() {
		return eName;
	}
	public String getEPhone() {
		return ePhone;
	}
	public String getFName () {
		return first;
	}
	
}
