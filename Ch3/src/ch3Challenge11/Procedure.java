package ch3Challenge11;

public class Procedure {
	private String procedure;
	private String date;
	private String practitioner;
	private double charge;
	
	public Procedure(String procedureName, String procedureDate, String procedurePractitioner, double procedureCharge) {
		procedure = procedureName;
		date = procedureDate;
		practitioner = procedurePractitioner;
		charge = procedureCharge;
	}
	public void setProcedure(String procedureName) {
		procedure = procedureName;
	}
	public String getProcedure() {
		return procedure;
	}
	public void setDate(String procedureDate) {
		date = procedureDate;
	}
	
	public String getDate() {
		return date;
	}
	public void setPractitioner(String procedurePractitioner) {
		practitioner = procedurePractitioner;
	}
	
	public String getPractitioner() {
		return practitioner;
	}
	public void setCharge(Double procedureCharge) {
		charge = procedureCharge;
	}
	public double getCharge() {
		return charge;
	}
}
