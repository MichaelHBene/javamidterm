package ch3Challenge11;

public class PatientDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Patient patient1 = new Patient("Jeff", "Don", "Smith", "123 Fake Street Dr.", "St. Louis", "MO", "63128", "(314) 123-1234","Becky Fakename","(314) 321-4321");
		Procedure procedure1 = new Procedure("Tongue reduction surgery", "Nov. 3, 1956", "Dr. Feelgood", 5000.25);
		Procedure procedure2 = new Procedure("Tongue enlargement surgery", "Nov. 4, 1956", "Dr. Mario", 5000.50);
		Procedure procedure3 = new Procedure("Tongue removal surgery", "Nov. 5, 1956", "Dr. Dan", 5.75);
		
		System.out.println("Patient Name:\n" + patient1.getFName() + " " + patient1.getMName() + " " +patient1.getLName() + "\n\nPatient Address:\n" + patient1.getStreet() + " " + patient1.getCity() + ", " + patient1.getState() + " " + patient1.getZip() + "\n\nPatient Phone Number:\n" + patient1.getPhone() + "\n\nEmergency Contact:\n" + patient1.getEName() + "\n" + patient1.getEPhone());
		System.out.println("\nProcedure #1:\n===================\nProcedure Name:\n" + procedure1.getProcedure() + "\nDate:\n" + procedure1.getDate() + "\nPractitioner:\n" + procedure1.getPractitioner() + "\nCharge:\n$" + procedure1.getCharge());
		System.out.println("\nProcedure #2:\n===================\nProcedure Name:\n" + procedure2.getProcedure() + "\nDate:\n" + procedure2.getDate() + "\nPractitioner:\n" + procedure2.getPractitioner() + "\nCharge:\n$" + procedure2.getCharge());
		System.out.println("\nProcedure #3:\n===================\nProcedure Name:\n" + procedure3.getProcedure() + "\nDate:\n" + procedure3.getDate() + "\nPractitioner:\n" + procedure3.getPractitioner() + "\nCharge:\n$" + procedure3.getCharge());
	}

}
