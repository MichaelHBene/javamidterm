package ch3Challenge10;

import java.util.Scanner;
public class PetDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner (System.in);
		
		System.out.println("Please enter the name of your pet:");
		String petName = keyboard.nextLine();
		System.out.println("Please enter the type of pet you have:");
		String petType = keyboard.nextLine();
		System.out.println("Please enter the age of your pet:");
		int petAge = keyboard.nextInt();
		
		Pet pet1 = new Pet();
		
		pet1.setName(petName);
		pet1.setType(petType);
		pet1.setAge(petAge);
		
		System.out.println("Your pet's name is " + pet1.getName() + ".\nIt is a " + pet1.getType() + ".\nIt's age is " + pet1.getAge() + " years.");
		
		
		
		
	}

}
