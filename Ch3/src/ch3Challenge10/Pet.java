package ch3Challenge10;

public class Pet {
	private String name;
	private String type;
	private int age;
	
	public void setName(String petName) {
		name = petName;
	}
	
	public void setType(String petType) {
		type = petType;
	}
	
	public void setAge(int petAge) {
		age = petAge;
	}
	
	public String getName() {
		return name;
	}
	
	public String getType() {
		return type;
	}
	
	public int getAge() {
		return age;
	}
}
