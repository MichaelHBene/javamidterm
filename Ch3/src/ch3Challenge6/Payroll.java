package ch3Challenge6;

public class Payroll {
	private String name;
	private int id;
	private double payRate;
	private double hoursWorked;
	
	public Payroll(String employeeName, int employeeID) {
		name = employeeName;
		id = employeeID;
	}
	public String getName() {
		return name;
	}
	public int getID() {
		return id;
	}
	public void setPayRate(double employeeRate) {
		payRate = employeeRate;
	}
	public double getPayRate() {
		return payRate;
	}
	public void setHours(double employeeHours) {
		hoursWorked = employeeHours;
	}
	public double getHours() {
		return hoursWorked;
	}

	public double getGrossPay() {
		double grossPay = hoursWorked * payRate;
		return grossPay;
	}
}	
