package ch3Challenge3;

public class personalInfo {
	private String name;
	private String address;
	private int age;
	private String phoneNumber;
	
	public void setName(String personName) {
		name = personName;
	}
	
	public String getName() {
		return name;
	}
	
	public void setAddress(String personAddress) {
		address = personAddress;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAge(int personAge) {
		age = personAge;
	}
	
	public int getAge() {
		return age;
	}
	
	public void setPhone(String personPhone) {
		phoneNumber = personPhone;
	}
	
	public String getPhone() {
		return phoneNumber;
	}
	
}
