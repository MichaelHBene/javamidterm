package ch3Challenge9;

import java.util.Scanner;
public class CircleDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner (System.in);
		
		System.out.println("Please enter the radius of the circle:");
		double radius = keyboard.nextDouble();
		
		Circle circle1 = new Circle(radius);
		
		System.out.println("The area of the circle is: " + circle1.getArea());
		System.out.println("The diameter of the circle is: " + circle1.getDiameter());
		System.out.println("The circumference of the circle is: " + circle1.getCircumference());
		
	}

}
