package Ch9Challenge2;

public class ShiftSupervisor extends Employee{

	private double salary;
	private double bonus;
	
	public ShiftSupervisor(String n, String num, String date, double sal, double b) {
		super(n, num, date);
		salary = sal;
		bonus = b;
	}
	
	public ShiftSupervisor() {
		
	}
	
	public void setSalary(double s) {
		salary = s;
	}
	
	public void setBonus(double b) {
		bonus = b;
	}
	
	public double getSalary() {
		return salary;
	}
	
	public double getBonus() {
		return bonus;
	}
	
	public String toString() {
		return ("Employee #" + super.getEmployeeNumber() + ": " + super.getName() + "\nHired: " + super.getHireDate() + "\nSalary: $" + this.getSalary() + "\nBonus: $" + this.getBonus());
	}
}
