package Ch9Challenge4;

public class Essay extends GradedActivity{
	private double grammer;
	private double spelling;
	private double correctLength;
	private double content;
	
	public void setScore(double gr, double sp, double len, double cnt) {
		grammer = gr;
		spelling = sp;
		correctLength = len;
		content = cnt;
		
		super.setScore((grammer + spelling + correctLength + content));
		
	}
	
	public void setGrammmer(double g) {
		grammer = g;
	}
	
	public void setSpelling(double s) {
		spelling = s;
	}
	
	public void setCorrectLength(double c) {
		correctLength = c;
	}
	
	public void setContent(double c) {
		content = c;
	}
	
	public double getGrammer() {
		return grammer;
	}
	
	public double getSpelling() {
		return spelling;
	}
	
	public double getCorrectLength() {
		return correctLength;
	}
	
	public double getScore() {
		return super.getScore();
	
	}
}
