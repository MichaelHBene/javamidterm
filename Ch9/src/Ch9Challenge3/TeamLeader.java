package Ch9Challenge3;

public class TeamLeader extends ProductionWorker{
	private double monthlyBonus;
	private double requiredTrainingHours;
	public double trainingHoursAttended;
	
	
	public TeamLeader(String n, String num, String date, int sh, double rate, double mb, double rth, double tha) {
		super(n, num, date, sh, rate);
		monthlyBonus = mb;
		requiredTrainingHours = rth;
		trainingHoursAttended = tha;
	}
	
	public TeamLeader() {
		
	}
	
	public void setMonthlyBonus(double b) {
		monthlyBonus = b;
	}
	
	public void setRequiredTrainingHours(double rth) {
		requiredTrainingHours = rth;
	}
	
	public void setTrainingHoursAttended(double tha) {
		trainingHoursAttended = tha;
	}
	
	public double getMonthlyBonus() {
		return monthlyBonus;
	}
	
	public double getRequiredTrainingHours() {
		return requiredTrainingHours;
	}
	
	public double getTrainingHoursAttended() {
		return trainingHoursAttended;
	}
	
	public String toString() {
		String str = "";
		str += super.toString();
		str += "\nMonthly Bonus: $" + this.getMonthlyBonus() + "\nRequired Training Hours: " + this.getRequiredTrainingHours() + "\nTraining Hours Attended: " + this.getTrainingHoursAttended();
		return str;
	}
}
