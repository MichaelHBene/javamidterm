package Ch9Challenge3;

public class Employee{
	private String name;
	private String employeeNumber;
	private String hireDate;
	
	public Employee(String n, String num, String date) {
		name = n;
		if(isValidEmpNum(num)) {
			employeeNumber = num;	
		}
		else {
			employeeNumber = "000-A";
		}
		hireDate = date;
	}
	
	public Employee() {
		
	}
	
	public void setName(String n) {
		name = n;
	}
	
	public void setEmployeeNumber(String e) {
		employeeNumber = e;
	}
	
	public void setHireDate(String h) {
		hireDate = h;
	}
	public String getName() {
		return name;
	}
	public String getEmployeeNumber() {
		return employeeNumber;
	}
	public String getHireDate() {
		return hireDate;
	}

	private boolean isValidEmpNum(String e) {
		if(e.matches("\\d{3}-\\w{1}")) {
			if(!e.substring(4).matches("[a-mA-M]") ) {
				return false;
			}
			else {
				return true;			
			}

		}
		else {
			return false;
		}
	}

	public String toString() {
		return "The employee is: ";
	}

}
