package Ch9Challenge5;

public class GradedActivity {
	private double score;
	
	public void setScore(double s) {
		score = s;
	}
	
	public double getScore() {
		return score;
	}
	
	public char getGrade() {
		char grade = 'A';
		if(score >= 0 && score <= 100) {
			if(score <= 100 & score >= 90) {
				grade = 'A';
			}
			else if(score <= 89 & score >= 80) {
				grade = 'B';
			}
			else if(score <= 79 & score >= 70) {
				grade = 'C';
			}
			else if(score <= 69 & score >= 60) {
				grade = 'D';
			}
			else if(score <= 59 & score >= 0) {
				grade = 'F';
			}
		}
		else {
			System.out.println("Assignment cannot be above 100 points, setting grade to 'N' for 'Needs Review'.");
			grade = 'N';
		}
		return grade;
	}
}
