package Ch9Challenge5;

public class CourseGrades {
	
	private int NUM_GRADES = 4;
	private GradedActivity[] grades = new GradedActivity[NUM_GRADES];
	
	public CourseGrades() {
		
	}
	
	public void setLab(GradedActivity aLab) {
		grades[0] = aLab;
	}
	
	public void setPassFailExam(PassFailExam aPassFailExam) {
		grades[1] = aPassFailExam;
	}
	
	public void setEssay(Essay anEssay) {
		grades[2] = anEssay;
	}
	
	public void setFinalExam(FinalExam aFinalExam) {
		grades[3] = aFinalExam;
	}
	
	public String toString() {
		return "Grades:\n" + "Lab: " + grades[0].getGrade() + " " + grades[0].getScore() + "\nPass/Fail Exam: " + grades[1].getGrade() + " " + grades[1].getScore() + "\nEssay: " + grades[2].getGrade() + " " + grades[2].getScore() + "\nFinal Exam: " + grades[3].getGrade() + " " + grades[3].getScore() + "\nALL GRADES ARE FINAL\nNO EXCEPTIONS!!!";
	}
	
}
