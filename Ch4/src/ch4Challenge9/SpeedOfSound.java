package ch4Challenge9;

public class SpeedOfSound {
	private int distance;
	
	public void setDistance(int distanceInFeet) {
		distance = distanceInFeet;
	}
	public double getDistance() {
		return distance;
	}
	
	public String getSpeedInAir() {
		double time = (double)distance/1100;
		return "It will take " + time + " seconds to travel that distance through air";
	}
	
	public String getSpeedInWater() {
		double time = (double)distance / 4900;
		return "It will take " + time + " seconds to travel that distance through water";
	}
	
	public String getSpeedInSteel() {
		double time = (double)distance / 16400;
		return "It will take " + time + " seconds to travel that distance through steel";
	}
}
