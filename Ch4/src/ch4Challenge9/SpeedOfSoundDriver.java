package ch4Challenge9;

import java.util.Scanner;
public class SpeedOfSoundDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SpeedOfSound sound = new SpeedOfSound();
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Please select either air, water, or steel");
		String material = keyboard.nextLine();
		System.out.println("Please enter the distance the sound wave will travel through the " + material);
		int distance = keyboard.nextInt();
		sound.setDistance(distance);
		if(material.equalsIgnoreCase("air")) {
			System.out.println(sound.getSpeedInAir());
		}
		else if(material.equalsIgnoreCase("water")) {
			System.out.println(sound.getSpeedInWater());
		}
		else if(material.equalsIgnoreCase("steel")) {
			System.out.println(sound.getSpeedInSteel());
		}
		else {
			System.out.println("Error, invalid material.");
		}
	}

}
