package ch4Challenge7;

public class FatGram {
	private int numFat;
	private int numCal;
	private int calFromFat;
	private double calPercent;
	
	public void setFat(int fat) {
		numFat = fat;
	}
	public void setCal(int cal) {
		numCal = cal;
	}
	public String calcCalFromFat() {
		calFromFat = numFat * 9;
		if(calFromFat > numCal) {
			return "Error. Calories from fat cannot exceed total calories.";
		}
		else {
			return "The number of calories from fat is: " + calFromFat;
		}
	}
	
	public String calcCalPercent() {
		calPercent = ((double)calFromFat / (double)numCal) * 10;
		String lowFat = "";
		if(calPercent < 30) {
			lowFat = "The food is low in fat!";
		}
		
		return "The total percent of calories from fat is: " + calPercent + "%. " + lowFat;
		
	}
	
}
