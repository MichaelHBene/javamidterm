package ch4Challenge15;

import java.util.Scanner;
public class BookClubDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Please enter the number of books you purchased");
		int books = keyboard.nextInt();
		int points = 0;
		if(books == 0) {
			points = 0;
		}
		else if(books == 1) {
			points = 5;
		}
		else if(books == 2) {
			points = 15;
		}
		else if(books == 3) {
			points = 30;
		}
		else if(books >= 4) {
			points = 60;
		}
		
		System.out.println("You've purchased " + books + " books this month, earning you " + points + " points!");
	}

}
