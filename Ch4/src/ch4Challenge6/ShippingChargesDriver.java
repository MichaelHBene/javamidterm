package ch4Challenge6;

import java.util.Scanner;
public class ShippingChargesDriver {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner (System.in);
		ShippingCharges delivery = new ShippingCharges();
	
		System.out.println("Please enter the weight of your package in Kilograms:");
		int shippingWeight = keyboard.nextInt();
		
		delivery.setWeight(shippingWeight);
		
		System.out.println(delivery.calcShipCharges());
		
	}
}
