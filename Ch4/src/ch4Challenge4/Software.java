package ch4Challenge4;

public class Software {
	private int unitsSold;
	private int packageCost = 99;
	private double discountPercent;
	private double discountTotal;
	
	public void setUnitsSold(int units) {
		unitsSold = units;
	}
	
	public String discountCost() {
		if(unitsSold >= 100) {
			discountPercent = .5;
			discountTotal = (double)(unitsSold * packageCost) * discountPercent;
			return "Your discount for this purchase is 50%";
		}
		else if(unitsSold >= 50) {
			discountPercent = .4;
			discountTotal = (double)(unitsSold * packageCost) * discountPercent;
			return "Your discount for this purchase is 40%";
		}
		else if(unitsSold >= 20) {
			discountPercent = .3;
			discountTotal = (double)(unitsSold * packageCost) * discountPercent;
			return "Your discount for this purchase is 30%";
		}
		else if(unitsSold >= 10) {
			discountPercent = .2;
			discountTotal = (double)(unitsSold * packageCost) * discountPercent;
			return "Your discount for this purchase is 20%";
		}
		else{
			discountPercent = 1;
			return "Purchase more software to save!";
		}
		
		
	}
	
	public String getTotalCost() {
		double totalCost = (unitsSold * packageCost) - discountTotal;
		
		return ("Your total cost for this purchase is: $" + totalCost);
	}
}
