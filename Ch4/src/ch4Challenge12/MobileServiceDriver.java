package ch4Challenge12;

import java.util.Scanner;

public class MobileServiceDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MobileService mobile = new MobileService();
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Please enter your preferred mobile plan (A, B, or C):");
		mobile.setPackage(keyboard.nextLine());
		System.out.println("Please enter the amount of minutes you used:");
		mobile.setMinutes(keyboard.nextInt());
		
		System.out.println(mobile.getCharge());
		
	}

}