package ch4Challenge8;

public class RunningRace {
	private String runner1Name = "Jerry";
	private int runner1Minutes = 21;
	private String runner2Name = "Jenny";
	private int runner2Minutes = 20;
	private String runner3Name = "Joe";
	private int runner3Minutes = 19;
	
	public String getRaceResults() {
		String first = "";
		String second = "";
		String third = "";
		
		if(runner1Minutes < runner2Minutes) {
			if(runner1Minutes < runner3Minutes) {
				first = runner1Name;
				if(runner2Minutes < runner3Minutes) {
					second = runner2Name;
					third = runner3Name;
				}
				else {
					second = runner3Name;
					third = runner2Name;
				}
			}
		}else if(runner2Minutes < runner3Minutes) {
			first = runner2Name;
			if(runner1Minutes < runner3Minutes) {
				second = runner1Name;
				third = runner3Name;
			}
			else {
				second = runner3Name;
				third = runner1Name;
			}
		}
		else {
			first = runner3Name;
			if(runner1Minutes < runner2Minutes) {
				second = runner1Name;
				third = runner2Name;
			}
			else {
				second = runner2Name;
				third = runner1Name;
			}
		}
		return "In first place is: " + first + "\nIn second place is: " + second + "\nIn third place is: " + third;
	}
	
}
