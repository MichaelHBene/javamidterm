package ch4Challenge5;

import java.util.Scanner;

import ch4Challenge4.Software;
public class BankChargesDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner (System.in);
		BankCharges account = new BankCharges();
		
		System.out.println("Please enter your ending balance for the month:");
		double endBalance = keyboard.nextDouble();
		account.setEndingBalance(endBalance);
		
		System.out.println("Please enter the number of checks written this month:");
		int numChecks = keyboard.nextInt();
		account.setChecks(numChecks);
		
		System.out.println(account.calcServiceCharge());
	}

}
