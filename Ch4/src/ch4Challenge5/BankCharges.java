package ch4Challenge5;

public class BankCharges {
	private int baseCharge = 10;
	private int numChecks;
	private double checkCharge;
	private double endingBalance;
	private int balanceCharge;
	
	public void setChecks(int checks) {
		numChecks = checks;
		this.getCheckCharge();
	}
	
	public void getCheckCharge() {
		if(numChecks >= 60) {
			checkCharge = .04;
		}
		else if(numChecks >= 40) {
			checkCharge = .06;
		}
		else if(numChecks >= 20) {
			checkCharge = .08;
		}
		else if(numChecks < 20) {
			checkCharge = .1;
		}
	}
	
	public void setEndingBalance(double balance) {
		if(balance < 400) {
			balanceCharge = 15;
		}
		else {
			balanceCharge = 0;
		}
	}
	
	public String calcServiceCharge() {
		double serviceCharge = ((double)baseCharge + (double)((double)numChecks*(double)checkCharge) + (double)balanceCharge);
		return("Your service charge for this month is: $" + serviceCharge);
	}
	
}
