package ch4Challenge16;

public class MagicDate {
	private int month;
	private int day;
	private int year;
	
	public MagicDate(int monthInt, int dayInt, int yearInt) {
		month = monthInt;
		day = dayInt;
		year = yearInt;
	}
	
	public boolean isMagic() {
		boolean isMagicDate = false;
		if((month * day) == year) {
			isMagicDate = true;
		}
		return isMagicDate;
	}
}
