package ch4Challenge16;

import java.util.Scanner;
public class MagicDateDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Please enter an int for a month");
		int month = keyboard.nextInt();
		System.out.println("Please enter an int for a day");
		int day = keyboard.nextInt();
		System.out.println("Please enter an int for a year");
		int year = keyboard.nextInt();
		MagicDate date = new MagicDate(month, day, year);
		if(date.isMagic()) {
			System.out.println("The date " + month + "/" + day + "/" + year + " is a magic date!");
		}
		else {
			System.out.println("The date " + month + "/" + day + "/" + year + " is not a magic date!");
		}
	}

}
