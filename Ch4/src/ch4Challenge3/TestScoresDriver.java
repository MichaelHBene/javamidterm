package ch4Challenge3;

import java.util.Scanner;
public class TestScoresDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner (System.in);
		TestScores scores = new TestScores();
		System.out.println("Please enter the score for Test #1:");
		scores.setTest1(keyboard.nextDouble());
		System.out.println("Please enter the score for Test #2:");
		scores.setTest2(keyboard.nextDouble());
		System.out.println("Please enter the score for Test #3:");
		scores.setTest3(keyboard.nextDouble());
		
		System.out.println(scores.getTestAverage());
		System.out.println(scores.getLetterGrade(scores.getTestAverage()));
	}

}
