package ch4Challenge14;

public class MonthDays {
	private int month;
	private int year;
	public MonthDays(int monthInt, int yearInt) {
		month = monthInt;
		year = yearInt;
	}
	
	public int getNumberOfDays() {
		boolean isLeap = false;
		if(year % 100 == 0) {
			if(year % 400 == 0) {
				isLeap = true;
			}
		}
		else if(year % 4 == 0) {
			isLeap = true;
		}
		switch(month) {
			case 1:
				if(isLeap) {
					return 31;
				}
			case 2:
				if(isLeap) {
					return 29;
				}
				else {
					return 28;
				}
			case 3:
				if(isLeap) {
					return 31;
				}
			case 4:
				if(isLeap) {
					return 30;
				}
			case 5:
				if(isLeap) {
					return 31;
				}
			case 6:
				if(isLeap) {
					return 30;
				}
			case 7:
				if(isLeap) {
					return 31;
				}
			case 8:
				if(isLeap) {
					return 31;
				}
			case 9:
				if(isLeap) {
					return 30;
				}
			case 10:
				if(isLeap) {
					return 31;
				}
			case 11:
				if(isLeap) {
					return 30;
				}
			case 12:
				if(isLeap) {
					return 31;
				}
			default:
				return 31;
				
		}
	}
}
