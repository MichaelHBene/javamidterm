package ch4Challenge14;

import java.util.Scanner;
public class MonthDaysDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Please enter the integer of a month:");
		int month = keyboard.nextInt();
		System.out.println("Please enter the interger of a year:");
		int year = keyboard.nextInt();
		
		MonthDays monthDays = new MonthDays(month, year);
		
		System.out.println("In the year " + year + ", the month will have " + monthDays.getNumberOfDays() + " days.");
	}

}
