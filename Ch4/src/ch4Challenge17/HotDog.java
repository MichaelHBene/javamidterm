package ch4Challenge17;

public class HotDog {
	private int people;
	private int dogsPerPerson;
	private int dogsPerPack = 10;
	private int bunsPerPack = 8;
	
	public void setPeople(int numPeople) {
		people = numPeople;
	}
	
	public void setDogs(int dogs) {
		dogsPerPerson = dogs;
	}
	
	public String calcPackages() {

		int dogsNeeded = people * dogsPerPerson;
		int bunsNeeded = dogsNeeded;
		
		int dogPackages = dogsNeeded / dogsPerPack;
		int bunPackages = bunsNeeded / bunsPerPack;
		
		int remainingDogsInPack = dogsNeeded % dogsPerPack;
		int remainingBunsInPack = bunsNeeded % bunsPerPack;
		
		int minimumDogPackages;
		int minimumBunPackages;
		
		if(remainingDogsInPack > 0) {
			minimumDogPackages = dogPackages + 1;
		}
		else {
			minimumDogPackages = dogPackages;
		}
		
		if(remainingBunsInPack > 0) {
			minimumBunPackages = bunPackages + 1;
		}
		else {
			minimumBunPackages = bunPackages;
		}
		
		int dogsLeftOver = (minimumDogPackages - dogPackages) * dogsPerPack;
		int bunsLeftOver = (minimumBunPackages - bunPackages) * bunsPerPack;
		
		
		
		return "To serve " + people + " people " + dogsPerPerson + " hot dog(s) each, you will need:\n" + minimumDogPackages + " Packages of hot dogs\nand\n" + minimumBunPackages + " Packages of buns.\nYou will have " + dogsLeftOver + " hot dogs and " + bunsLeftOver + " buns left over.";  
		
	}
}
