package ch4Challenge13;

public class BMI {
	private double weight;
	private double height;
	
	public void setWeight(double weightInput) {
		weight = weightInput;
	}
	
	public void setHeight(double heightInput) {
		height = heightInput;
	}
	
	public String calcBMI() {
		double BMI = weight * (703/(height*height));
		String weightString = "";
		if(BMI > 25) {
			weightString = "Overweight";
		}
		else if(BMI < 18.5) {
			weightString = "Underweight";
		}
		else {
			weightString = "Optimal";
		}
		return weightString;
	}
}
