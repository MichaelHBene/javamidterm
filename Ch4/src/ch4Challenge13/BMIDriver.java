package ch4Challenge13;

import java.util.Scanner;
public class BMIDriver {
	public static void main(String[] args) {
		BMI bmi = new BMI();
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Please enter your weight in pounds:");
		bmi.setWeight(keyboard.nextDouble());
		System.out.println("Please enter your height in inches:");
		bmi.setHeight(keyboard.nextDouble());
		
		System.out.println(bmi.calcBMI());
	
	}
}
