package ch4Challenge18;

public class RouletteWheelColors {
	private int pocket;
	public RouletteWheelColors(int pocketNum) {
		pocket = pocketNum;
	}
	
	public String getPocketColor() {
		if(pocket <= 36) {
			if(pocket < 29) {
				if(pocket < 19) {
					if(pocket < 11) {
						if(pocket < 1) {
							if(pocket < 0) {
								return "Invalid Pocket!";
							}
							else {
								return "The color is green";
							}
						}
						else if(pocket % 2 == 0) {
							return "The color is black";
							
						}
						else {
							return "The color is red";
						}
					}
					else if(pocket % 2 == 0) {
						return "The color is red";
						
					}
					else {
						return "The color is black";
					}
				}
				else if(pocket % 2 == 0) {
					return "The color is black";
					
				}
				else {
					return "The color is red";
				}
			}
			else if(pocket % 2 == 0) {
				return "The color is red";
				
			}
			else {
				return "The color is black";
			}
		}
		else {
			return "Invalid Pocket!";
		}
	}
}
