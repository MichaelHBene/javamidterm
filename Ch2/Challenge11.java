import java.util.Scanner;
public class Challenge11
{
    public static void main (String[] args)
    {
        Scanner keyboard = new Scanner (System.in);
        System.out.println("Please enter the number of male students in your class:");
        int numMales = keyboard.nextInt();
        System.out.println("Please enter the number of female students in your class:");
        int numFemales = keyboard.nextInt();
        int totalStudents = numMales + numFemales;
        double malePercent = (double)numMales / (double)totalStudents;
        double femalePercent = (double)numFemales / (double)totalStudents;
        
        System.out.println("There are " + totalStudents + " students in the class. " + numMales + " students are male and " + numFemales + " students are female.\n The percentage of males is " + malePercent + "% and the percentage of females is " + femalePercent + "%");
    }
}