import java.util.Scanner;
public class Challenge13
{
    public static void main (String[] args)
    {
        Scanner keyboard = new Scanner (System.in);
        
        System.out.println("Enter the cost of your meal:");
        double mealCost = keyboard.nextDouble();
        double tipPercent = .18;
        double tipAmount = mealCost * tipPercent;
        double taxPercent = .075;
        double taxAmount = mealCost * taxPercent;
        double totalCost = mealCost + tipAmount + taxAmount;
        
        System.out.println("|======================================|\n|              Your Bill:              |\n|======================================|\n Meal Cost: " + mealCost + "\n Tip: " + tipAmount + "\n Tax: " + taxAmount + "\n|--------------------------------------|\n Total: $" + totalCost);
        
    }
}