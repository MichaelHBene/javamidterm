import java.util.Scanner;
public class Challenge12
{
    public static void main (String[] args)
    {
        Scanner keyboard = new Scanner (System.in);
        
        System.out.println("Enter the name of your favorite city:");
        String cityName = keyboard.nextLine();
        int numLetters = cityName.length();
        
        System.out.println("Here's your city:\n "+ numLetters + " letters in the city\n " + cityName.toUpperCase() + "\n "+ cityName.toLowerCase() + "\n " + cityName.substring(0,1));
        
    }
}