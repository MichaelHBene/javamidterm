import java.util.Scanner;
public class Challenge8
{
    public static void main (String[] args)
    {
        Scanner keyboard = new Scanner (System.in);
        
        System.out.println("Please enter the amount of your purchase: ");
        double amountPurchased = keyboard.nextDouble();
        double stateTax = .055 * amountPurchased;
        double countyTax = .02 * amountPurchased;
        double totalCost = amountPurchased + stateTax + countyTax;
        System.out.println("Your purchase was $" + amountPurchased + " with a state sales tax of $" + stateTax + " and a county sales tax of $" + countyTax + ". Your total cost today is $" + totalCost);
    }
}