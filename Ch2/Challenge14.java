public class Challenge14
{
    public static void main (String[] args)
    {
        double stockCost = 25.50;
        int numberPurchased = 1000;
        double commissionRate = .02;
        double stockTotal = stockCost * numberPurchased;
        double commissionTotal = stockTotal * commissionRate;
        double totalCost = stockTotal + commissionTotal;
        
        System.out.println("Total Cost of Stocks: \n$" + stockTotal + "\nTotal Cost of Commission: \n$" + commissionTotal + "\nOverall Cost: \n$" + totalCost);
        
    }
}