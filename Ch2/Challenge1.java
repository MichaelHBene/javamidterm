public class Challenge1
{
    public static void main (String[] args)
    {
        String name = "Jeff";
        int age = 5;
        double annualPay = 0.01;
        System.out.println("My name is " + name + ", my age is " + age + " and I hope to earn $" + annualPay + " per year.");
    }
}