import java.util.Scanner;
public class Challenge17
{
    public static void main (String[] args)
    {
        Scanner keyboard = new Scanner (System.in);
        System.out.println("Welcome to Word Game. Please enter your NAME:");
        String name = keyboard.nextLine();
        System.out.println("Please enter your AGE:");
        String age = keyboard.nextLine();
        System.out.println("Please enter a CITY:");
        String city = keyboard.nextLine();
        System.out.println("Please enter a COLLEGE:");
        String college = keyboard.nextLine();
        System.out.println("Please enter a PROFESSION:");
        String profession = keyboard.nextLine();
        System.out.println("Please enter an ANIMAL:");
        String animal = keyboard.nextLine();
        System.out.println("Please enter a PET'S NAME:");
        String petName = keyboard.nextLine();
        
        System.out.println("\nHere's your story:\n-------------------------\n   There once was a person named " + name + " who lived in " + city + ". At the age of " + age + ", " + name + " went to college at " + college + ". " + name + " graduated and went to work as a " + profession + ". Then, " + name + " adopted a(n) " + animal + " named " + petName + ". They both lived happily ever after!");
    }
}