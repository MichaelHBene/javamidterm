
public class Challenge16
{
    public static void main (String[] args)
    {
        int numSurveyed = 15000;
        double addictPercent = .18;
        double juiceHeadPercent = .58;
        int numAddicts = (int)(numSurveyed * addictPercent);
        int numJuiceHeads = (int)(numSurveyed * juiceHeadPercent);
        
        System.out.println("Number of Energy Drink Addicts: " + numAddicts + "\nNumber of Juiceheads: " + numJuiceHeads);
    }
}