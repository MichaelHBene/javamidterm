public class Challenge6
{
    public static void main (String[] args)
    {
        double eastCoastSales = 8300000 * .65;
        
        System.out.println("The East Coast sales division made $" + eastCoastSales + " in revenue");
    }
}