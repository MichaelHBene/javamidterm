import java.util.Scanner;
public class Challenge9
{
    public static void main (String[] args)
    {
        Scanner keyboard = new Scanner (System.in);
        
        System.out.println("Please enter the miles you have driven: ");
        double numMiles = keyboard.nextDouble();
        System.out.println("Please enter the gallons of gas you consumed: ");
        double numGallons = keyboard.nextDouble();
        double calculatedMPG = numMiles / numGallons;
        System.out.println("You drove " + numMiles + " miles on " + numGallons + " gallons of gas. Your calculated MPG is: \n" + calculatedMPG + " Miles per gallon.");
    }
}