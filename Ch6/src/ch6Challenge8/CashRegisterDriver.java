package ch6Challenge8;

import java.util.Scanner;
public class CashRegisterDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Please enter the number of items you are purchasing:");
		int numItems = keyboard.nextInt();
		
		RetailItem item = new RetailItem("Some commercial good", 1, 49.99, 89.99);
		System.out.println(item.toString());
		
		CashRegister register = new CashRegister(item, numItems);
		
		System.out.printf("Subtotal: $%.2f\n", register.getSubtotal());
		System.out.printf("Sales Tax: $%.2f\n", register.getTax());
		System.out.printf("Total: $%.2f\n", register.getTotal());
		
		
	}

}
