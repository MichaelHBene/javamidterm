package ch6Challenge8;

public class CashRegister {
	private RetailItem item;
	private int itemsPurchased;
	private double subtotal;
	private double salesTax;
	private double total;
	public CashRegister(RetailItem retail, int numItems) {
		item = retail;
		itemsPurchased = numItems;
	}
	
	public double getSubtotal() {
		subtotal = (item.getRetail() * itemsPurchased);
		return subtotal;
	}
	
	public double getTax() {
		salesTax = (subtotal * .06);
		return salesTax;
	}
	
	public double getTotal() {
		total = subtotal + salesTax;
		return total;
	}
	
}
