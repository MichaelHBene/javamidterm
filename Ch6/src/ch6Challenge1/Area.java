package ch6Challenge1;

public class Area {

	public static double CalcArea(double radius, double height) {
		double area = Math.PI * (radius * radius) * height;
		return area;
	}
	
	public static double CalcArea(double radius) {
		double area = Math.PI * (radius * radius);
		return area;
	}
	
	public static int CalcArea(int width, int height) {
		int area = width * height;
		return area;
	}
}
