package ch6Challenge1;

public class AreaDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Area rectangle = new Area();
		int rectangleArea = rectangle.CalcArea(5, 10);
		
		Area circle = new Area();
		double circleArea = circle.CalcArea(3.7);
		
		Area cylinder = new Area();
		double cylinderArea = cylinder.CalcArea(5, 5.5);
		
		System.out.println("The area of the rectangle is: " + rectangleArea);
		System.out.println("The area of the circle is: " + circleArea);
		System.out.println("The area of the cylinder is: " + cylinderArea);
		
		
	}
	
	

}
