package ch6Challenge14;

import java.util.Random;

public class Coin {
	
	private int sides;
	private int value;
	
	public Coin(int numSides) {
		sides = numSides;
		flip();
	}
	
	public void flip() {
		Random rand = new Random();
		value = rand.nextInt(sides) + 1;
	}
	
	public int getSides() {
		return sides;
	}
	
	public int getValue() {
		return value;
	}
}
