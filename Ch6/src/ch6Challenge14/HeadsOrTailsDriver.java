package ch6Challenge14;

import java.util.Scanner;
public class HeadsOrTailsDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Player player1 = new Player(0, "Jim");
		Player player2 = new Player(0, "Sarah");
		
		Coin quarter = new Coin(2);
		
		Scanner keyboard = new Scanner(System.in);
		
		int player1Guess;
		int player2Guess;
		
		while(player1.getPoints() != 5 && player2.getPoints() != 5) {
			
			System.out.println(player1.getName() + ", please enter your guess, heads or tails? (1 for heads, 2 for tails):");
			player1Guess = keyboard.nextInt();
			if(player1Guess != 1 && player1Guess != 2) {
				System.out.println("Incorrect Input. You have instead guessed heads.");
				player1Guess = 1;
			}
			System.out.println(player2.getName() + ", please enter your guess, heads or tails? (1 for heads, 2 for tails):");
			player2Guess = keyboard.nextInt();
			if(player2Guess != 1 && player2Guess != 2) {
				System.out.println("Incorrect Input. You have instead guessed heads.");
				player2Guess = 1;
			}
			
			quarter.flip();
			
			if(player1Guess == quarter.getValue()) {
				System.out.println(player1.getName() + " has earned a point for guessing correctly!");
				player1.addPoints(1);
				System.out.println(player1.getName() + "'s points: " + player1.getPoints());
			}
			else {
				if(player1.getPoints() == 0) {
					System.out.println(player1.getName() + " cannot lose any more points!");
				}
				else {
					System.out.println(player1.getName() + " has lost a point for guessing incorrectly!");
					player1.subtractPoints(1);	
				}
				System.out.println(player1.getName() + "'s points: " + player1.getPoints());
			}
			
			if(player2Guess == quarter.getValue()) {
				System.out.println(player2.getName() + " has earned a point for guessing correctly!");
				player2.addPoints(1);
				System.out.println(player2.getName() + "'s points: " + player2.getPoints());
			}
			else {
				if(player2.getPoints() == 0) {
					System.out.println(player2.getName() + " cannot lose any more points!");
				}
				else {
					System.out.println(player2.getName() + " has lost a point for guessing incorrectly!");
					player2.subtractPoints(1);	
				}
				System.out.println(player2.getName() + "'s points: " + player2.getPoints());
			}
			
			
		}
		
		if(player1.getPoints() == 5) {
			System.out.println(player1.getName() + " wins!");
		}
		
		if(player2.getPoints() == 5) {
			System.out.println(player2.getName() + " wins!");
		}
		
		
	}

}
