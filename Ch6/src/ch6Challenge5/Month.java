package ch6Challenge5;

public class Month {
	private int monthNumber;
	
	public Month() {
		monthNumber = 1;
	}
	
	public Month(int numberOfMonth) {
		if(numberOfMonth < 1 || numberOfMonth > 12) {
			monthNumber = 1;
		}
		else {
			monthNumber = numberOfMonth;			
		}
	}
	
	public Month(String nameOfMonth) {
		switch(nameOfMonth.toLowerCase()) {
		case "january":
			monthNumber = 1;
			break;
		case "february":
			monthNumber = 2;
			break;
		case "march":
			monthNumber = 3;
			break;
		case "april":
			monthNumber = 4;
			break;
		case "may":
			monthNumber = 5;
			break;
		case "june":
			monthNumber = 6;
			break;
		case "july":
			monthNumber = 7;
			break;
		case "august":
			monthNumber = 8;
			break;
		case "september":
			monthNumber = 9;
			break;
		case "october":
			monthNumber = 10;
			break;
		case "november":
			monthNumber = 11;
			break;
		case "december":
			monthNumber = 12;
			break;
		default:
			monthNumber = 1;
			break;
		}
	}
	
	public void setMonthNumber(int numberOfMonth) {
		if(numberOfMonth < 1 || numberOfMonth > 12) {
			monthNumber = 1;
		}
		else {
			monthNumber = numberOfMonth;			
		}
	}
	
	public int getMonthNumber() {
		return monthNumber;
	}
	public String getMonthName() {
		String monthName = "";
		switch(monthNumber) {
		case 1:
			monthName = "January";
			break;
		case 2:
			monthName = "February";
			break;
		case 3:
			monthName = "March";
			break;
		case 4:
			monthName = "April";
			break;
		case 5:
			monthName = "May";
			break;
		case 6:
			monthName = "June";
			break;
		case 7:
			monthName = "July";
			break;
		case 8:
			monthName = "August";
			break;
		case 9:
			monthName = "September";
			break;
		case 10:
			monthName = "October";
			break;
		case 11:
			monthName = "November";
			break;
		case 12:
			monthName = "December";
			break;
		default:
			monthName = "January";
			break;
		}
		return monthName;
	}
	
	public String toString() {
		String monthName = this.getMonthName();
		return monthName;
	}
	
	public boolean equals(Month month2) {
		boolean status = false;
		int month1Num = this.getMonthNumber();
		int month2Num = month2.getMonthNumber();
				
		if(month1Num == month2Num) {
			status = true;
		}
		return status;
	}
	
	public boolean lessThan(Month month2) {
		boolean status = false;
		int month1Num = this.getMonthNumber();
		int month2Num = month2.getMonthNumber();
				
		if(month1Num < month2Num) {
			status = true;
		}
		return status;
	}
}
