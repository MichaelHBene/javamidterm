package ch6Challenge5;

public class MonthDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Month month1 = new Month(4);
		System.out.println(month1.getMonthNumber());
		System.out.println(month1.getMonthName());
		
		Month month2 = new Month("september");
		System.out.println(month2.getMonthNumber());
		System.out.println(month2.toString());
		System.out.println(month1.equals(month2));
		System.out.println(month1.lessThan(month2));
		
		Month month3 = new Month();
		System.out.println(month3.getMonthNumber());
		System.out.println(month3.getMonthName());
	}

}
