package ch6Challenge12;

public class CarDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Odometer odie = new Odometer(0);
		FuelGauge carJuice = new FuelGauge(0);
		
		carJuice.fillUp();
		
		while (carJuice.getFuel() > 0) {
			odie.increaseMileage();
			if(odie.getMileage() % 24 == 0) {
				carJuice.burnFuel();	
			}
			
			System.out.println("Mileage: " + odie.getMileage());
			System.out.println("Fuel in Tank: " + carJuice.getFuel() + " /15");
			System.out.println("");
			
		}
		
	}

}
