package ch6Challenge12;

public class Odometer {
	private int mileage;
	
	public Odometer(int startingMileage) {
		mileage = startingMileage;
	}
	
	public int getMileage() {
		return mileage;
	}
	
	public int increaseMileage() {
		if(mileage < 999999) {
			++mileage;
		}
		else {
			mileage = 0;
		}
		
		return mileage;
	}
	
	public int burnFuel(FuelGauge gauge) {
		int numFuelToDecrease = mileage / 24;
		for(int x = 0; x <= numFuelToDecrease; ++x) {
			gauge.burnFuel();
		}
		return gauge.getFuel();
	}
}
