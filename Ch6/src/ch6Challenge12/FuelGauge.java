package ch6Challenge12;

public class FuelGauge {
	private int fuel;
	
	public FuelGauge(int startingFuel) {
		fuel = startingFuel;
	}
	
	public int getFuel() {
		return fuel;
	}
	
	public int fillUp() {
		while (fuel < 15) {
			++fuel;
		}
		return fuel;
	}
	
	public int burnFuel() {
		if(fuel > 0) {
			--fuel;
		}
		return fuel;
	}
}
