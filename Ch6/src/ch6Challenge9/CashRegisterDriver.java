package ch6Challenge9;
import java.io.*;
import java.util.Scanner;
public class CashRegisterDriver {

	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub
		
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Please enter the number of items you are purchasing:");
		int numItems = keyboard.nextInt();
		
		RetailItem item = new RetailItem("Some commercial good", 1, 49.99, 89.99);
		System.out.println(item.toString());
		
		CashRegister register = new CashRegister(item, numItems);
		
		System.out.printf("Subtotal: $%.2f\n", register.getSubtotal());
		System.out.printf("Sales Tax: $%.2f\n", register.getTax());
		System.out.printf("Total: $%.2f\n", register.getTotal());
		
		PrintWriter receipt = new PrintWriter("src\\ch6Challenge9\\Receipt.txt");
		
		receipt.println("SALES RECEIPT");
		receipt.printf("Unit Price: $%.2f", item.getRetail());
		receipt.println();
		receipt.printf("Quantity: " + numItems);
		receipt.println();
		receipt.printf("Subtotal: $%.2f", register.getSubtotal());
		receipt.println();
		receipt.printf("Sales Tax: $%.2f", register.getTax());
		receipt.println();
		receipt.printf("Total: $%.2f", register.getTotal());
		receipt.close();
		System.out.println("Your receipt has been printed.");
	}

}
