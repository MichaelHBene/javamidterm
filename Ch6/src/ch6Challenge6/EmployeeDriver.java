package ch6Challenge6;

public class EmployeeDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Employee emp1 = new Employee("Joe Dirt", 104, "Custodial", "Dirty Boy");
		Employee emp2 = new Employee("Burt Reynolds", 81);
		Employee emp3 = new Employee();
		
		System.out.println("Employee Name: " + emp1.getName());
		System.out.println("ID #: " + emp1.getIdNumber());
		System.out.println("Department: " + emp1.getDepartment());
		System.out.println("Position: " + emp1.getPosition());
		
		System.out.println("\n");
		
		System.out.println("Employee Name: " + emp2.getName());
		System.out.println("ID #: " + emp2.getIdNumber());
		System.out.println("Department: " + emp2.getDepartment());
		System.out.println("Position: " + emp2.getPosition());
		
		System.out.println("\n");
		
		System.out.println("Employee Name: " + emp3.getName());
		System.out.println("ID #: " + emp3.getIdNumber());
		System.out.println("Department: " + emp3.getDepartment());
		System.out.println("Position: " + emp3.getPosition());
		
	}

}
