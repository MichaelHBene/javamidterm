package ch6Challenge6;

public class Employee {
	private String Name;
	private int idNumber;
	private String department;
	private String position;
	
	public Employee(String empName, int empID, String empDept, String empPosition) {
		Name = empName;
		idNumber = empID;
		department = empDept;
		position = empPosition;
	}
	
	public Employee(String empName, int empID) {
		Name = empName;
		idNumber = empID;
		department = "";
		position = "";
	}
	
	public Employee() {
		Name = "";
		idNumber = 0;
		department = "";
		position = "";
	}
	
	public void setName(String employeeName) {
		Name = employeeName;
	}
	
	public String getName() {
		return Name;
	}
	
	public void setIdNumber(int id) {
		idNumber = id;
	}
	
	public int getIdNumber() {
		return idNumber;
	}
	
	public void setDepartment(String employeeDepartment) {
		department = employeeDepartment;
	}
	
	public String getDepartment() {
		return department;
	}
	
	public void setPosition(String employeePosition) {
		position = employeePosition;
	}
	
	public String getPosition() {
		return position;
	}
}
