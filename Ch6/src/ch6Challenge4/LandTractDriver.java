package ch6Challenge4;

import java.util.Scanner;
public class LandTractDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Enter the length of plot 1: ");
		int plot1Length = keyboard.nextInt();
		System.out.println("Enter the width of plot 1: ");
		int plot1Width = keyboard.nextInt();
		System.out.println("Enter the length of plot 2: ");
		int plot2Length = keyboard.nextInt();
		System.out.println("Enter the width of plot 2: ");
		int plot2Width = keyboard.nextInt();
		
		LandTract plot1 = new LandTract(plot1Length, plot1Width);
		LandTract plot2 = new LandTract(plot2Length, plot2Width);
		
		System.out.println(plot1.toString());
		System.out.println(plot2.toString());
		
		if(plot1.equals(plot2, plot1)) {
			System.out.println("The two tracts of land are the same area.");	
		}
		
	}

}
