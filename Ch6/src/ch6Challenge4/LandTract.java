package ch6Challenge4;

public class LandTract {
	private int tractLength;
	private int tractWidth;
	
	public LandTract(int length, int width) {
		tractLength = length;
		tractWidth = width;
	}
	
	public int getArea() {
		int area = tractLength * tractWidth;
		return area;
	}
	public String toString() {
		String displayString = ("The area of this plot of land is: " + this.getArea());
		return displayString;
	}
	
	public boolean equals(LandTract plot2, LandTract plot1) {
		boolean status = false;
		int plot1Area = plot1.getArea();
		int plot2Area = plot2.getArea();
				
		if(plot1Area == plot2Area) {
			status = true;
		}
		return status;
	}
	
}
