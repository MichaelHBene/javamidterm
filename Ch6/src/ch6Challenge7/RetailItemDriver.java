package ch6Challenge7;

public class RetailItemDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		RetailItem item = new RetailItem("Chainmail", 1, 124.99, 149.99);
		
		System.out.println(item.toString());
		
		item.setRetail(119.99);
		item.setWholesale(99.99);
		
		System.out.printf("Retail Price after sale: $%,.2f\n", item.getRetail());
		System.out.printf("Wholesale Price after sale: $%,.2f\n", item.getWholesale());
	}

}
