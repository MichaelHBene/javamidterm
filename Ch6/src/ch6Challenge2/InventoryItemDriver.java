package ch6Challenge2;

public class InventoryItemDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		InventoryItem item1 = new InventoryItem("Hello", 2);
		InventoryItem item2 = new InventoryItem (item1);
		
		System.out.println("The description for item 1 is: " + item1.getDescription());
		System.out.println("The units for item 1 is: " + item1.getUnits());
		
		System.out.println("The description for item 2 is: " + item2.getDescription());
		System.out.println("The units for item 2 is: " + item2.getUnits());
		
		
		
	}

}
