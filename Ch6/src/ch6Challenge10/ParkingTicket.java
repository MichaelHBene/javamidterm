package ch6Challenge10;

public class ParkingTicket {
	private String carMake;
	private String carModel;
	private String carColor;
	private String carPlate;
	private int meterMinutes;
	private int parkedMinutes;
	private String copName;
	private int copBadge;
	
	public ParkingTicket(ParkedCar evilCar, ParkingMeter goodMeter, PoliceOfficer cop) {
		carMake = evilCar.getMake();
		carModel = evilCar.getModel();
		carColor = evilCar.getColor();
		carPlate = evilCar.getLicense();
		parkedMinutes = evilCar.getMinutes();
		meterMinutes = goodMeter.getMinutes();
		copName = cop.getName();
		copBadge = cop.getBadge();
		
		this.getCar();
		this.getFine();
		this.getOfficer();
	}
	
	public void getCar() {
		System.out.println("Make: " + carMake + "\nModel: " + carModel + "\nColor: " + carColor + "\nLicense Plate: " + carPlate );
	}
	
	public void getFine() {
		
		int numMinutesExpired = parkedMinutes - meterMinutes;
		
		int cost = 0;
		
		if(numMinutesExpired > 0) {
			int numHours = numMinutesExpired / 60;
			
			if(numHours > 1) {
				cost = 25 + (10 * (numHours - 1));
			}
			else {
				cost = 25;
			}	
		}
		
		System.out.printf("Fine Amount: $" + cost + "\n");
		
	}
	
	public void getOfficer() {
		System.out.println("Officer " + copName + "\nBadge #: " + copBadge);
	}
}
