package ch6Challenge10;

public class PoliceOfficer {
	private String officer;
	private int badge;
	private boolean expired = false;
	private ParkingMeter meter;
	private ParkedCar car;
	
	public PoliceOfficer(ParkingMeter goodMeter, ParkedCar evilCar, String officerName, int badgeNum) {
		officer = officerName;
		badge = badgeNum;
		meter = goodMeter;
		car = evilCar;
		
		int meterMinutes = goodMeter.getMinutes();
		int parkedMinutes = evilCar.getMinutes();
		
		if(meterMinutes < parkedMinutes) {
			expired = true;
		}
		
		ParkingTicket ticket = new ParkingTicket(car, meter, this);
		
	}
	
	public String getName() {
		return officer;
	}
	
	public int getBadge() {
		return badge;
	}
	
}
