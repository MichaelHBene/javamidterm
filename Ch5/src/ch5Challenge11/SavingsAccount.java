package ch5Challenge11;

public class SavingsAccount {

	private double balance;
	private double annualInterestRate;
	private double monthlyInterestRate;
	
	public SavingsAccount(double startingBalance, double annualInterest) {
		balance = startingBalance;
		annualInterestRate = annualInterest;
		monthlyInterestRate = annualInterestRate / 12;
	}
	
	public double subtractBalance(double subtract) {
		balance -= subtract;
		return balance;
	}
	
	public double addBalance(double add) {
		balance += add;
		return balance;
	}
	
	public double addInterest() {	
		double interestEarned = balance * monthlyInterestRate;
		balance += interestEarned;
		return interestEarned;
	}
	
	public double getBalance() {
		return balance;
	}
}
