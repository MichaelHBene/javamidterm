package ch5Challenge11;

import java.util.Scanner;
import java.io.*;
public class SavingsAccountDriver {

	public static void main(String[] args) throws IOException{
		// TODO Auto-generated method stub

		Scanner keyboard = new Scanner(System.in);
		System.out.println("Please enter the annual interest rate of the account:");
		double interestRate = keyboard.nextDouble();
		System.out.println("Please enter the number of months that have passed since the account was established:");
		int months = keyboard.nextInt();
		
		SavingsAccount account = new SavingsAccount(500, interestRate);
		
		double totalDeposits = 0;
		double totalWithdrawls = 0;
		double totalInterest = 0;
		
		File deposit = new File("src\\ch5Challenge11\\Deposits.txt");
		Scanner depositFile = new Scanner(deposit);
		File withdrawal = new File("src\\ch5Challenge11\\Withdrawals.txt");
		Scanner withdrawalFile = new Scanner(withdrawal);
		
		
		for(int x = 1; x <= months; ++x) {
			if(depositFile.hasNext()) {
				double deposits = depositFile.nextDouble();
				totalDeposits += deposits;
				account.addBalance(deposits);
			}
			else {
				depositFile.close();
			}
			if(withdrawalFile.hasNext()) {
				double withdrawls = withdrawalFile.nextDouble();
				totalWithdrawls += withdrawls;
				account.subtractBalance(withdrawls);
			}
			else {
				withdrawalFile.close();
			}

			totalInterest += account.addInterest();
			
		}
		
		System.out.printf("The ending balance of the account is: $%.2f", account.getBalance());
		System.out.printf("\nThe total amount of deposits during this period was: $%.2f", totalDeposits);
		System.out.printf("\nThe total amount of withdrawls during this period was: $%.2f", totalWithdrawls);
		System.out.printf("\nThe total amount of interest earned during this period was: $%.2f", totalInterest);
		
		keyboard.close();
		
	}

}
