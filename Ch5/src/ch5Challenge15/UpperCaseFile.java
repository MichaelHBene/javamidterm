package ch5Challenge15;
import java.io.*;
import java.util.Scanner;
public class UpperCaseFile {
	
	public UpperCaseFile(String file1Input, String file2Input)throws IOException{
		File file = new File(file1Input);
		Scanner inputFile = new Scanner(file);
		
		PrintWriter outputFile = new PrintWriter(file2Input);
		
		while(inputFile.hasNext()) {
			String fileLine = inputFile.nextLine();
			System.out.println(fileLine);
			outputFile.println(fileLine.toUpperCase());
		}
		
		outputFile.close();
		inputFile.close();
	}
	
	
	
	
}
