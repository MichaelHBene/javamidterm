package ch5Challenge4;

import java.util.Scanner;
public class PenniesForPay {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Enter the number of days you worked:");
		Scanner keyboard = new Scanner(System.in);
		int numDays = keyboard.nextInt();
		if(numDays < 1) {
			String displaySalary = "Day #: | Number Pennies:";
			
			double numPennies = 0;
			double totalSalary = 0;
			
			for(int x = 1; x <= numDays; ++x) {
				if(numPennies == 0) {
					numPennies += 1;
				}
				else {
					numPennies *= 2;
				}
				
				double dollarDisplay = numPennies / 100;
				
				totalSalary += dollarDisplay;
				
				displaySalary += ("\n" + x + "        $" + dollarDisplay);
			}
			
			System.out.println(displaySalary + "\n");
			System.out.printf("The total salary is:\n$%.2f", totalSalary);	
		}
		
		else {
			System.out.println("Error. You cannot work less than 1 day.");
		}
		
		
	}

}
