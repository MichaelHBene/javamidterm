package ch5Challenge6;

public class Population {
	private int startingOrganisms;
	private double avgIncrease;
	private int incubationDays;
	
	public Population(int organisms, double increase, int days) {
		startingOrganisms = organisms;
		avgIncrease = increase;
		incubationDays = days;
	}
	
	public void calcPopulation() {
		int population = startingOrganisms;
		System.out.println("The starting population of the colony is: " + population);
		for(int x = 1; x <= incubationDays; ++x) {
			population += population * avgIncrease;
			System.out.println("The population for day #" + x + " is: " + population);
		}
	}
	
	
}
