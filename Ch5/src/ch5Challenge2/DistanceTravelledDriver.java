package ch5Challenge2;

import java.util.Scanner;
public class DistanceTravelledDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		boolean isValidated = true;
		
		System.out.println("Please enter the speed of the vehicle");
		double vehicleSpeed = keyboard.nextDouble();
		if(vehicleSpeed < 0) {
			System.out.println("Error, speed cannot be less than 0");
			isValidated = false;
		}
		System.out.println("Please enter the time travelled");
		int timeTravelled = keyboard.nextInt();
		if(timeTravelled < 1) {
			System.out.println("Error, time travelled cannot be less than 1");
			isValidated = false;
		}
		
		DistanceTravelled travel = new DistanceTravelled(vehicleSpeed, timeTravelled);
		
		if(isValidated = true) {
			for(int x = 1; x <= timeTravelled; ++x) {
				travel.setTime(x);
				System.out.println("Hour " + x + ":\n Distance Travelled: " + travel.getDistance());
			}
		}
	}

}
