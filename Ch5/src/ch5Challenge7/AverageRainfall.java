package ch5Challenge7;

import java.util.Scanner;
public class AverageRainfall {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Please enter the number of years:");
		
		int years = keyboard.nextInt();
		
		if(years >= 1) {
			
			int totalRainfall = 0;
			
			int monthCounter = 0;
			
			for(int x = 1; x <= years; ++x) {
				
				for(int i = 1; i <= 12; ++i) {
					
					System.out.println("Please enter the amount of rainfall this month:");
					int rainfall = keyboard.nextInt();
					
					if(rainfall >= 0) {
						totalRainfall += rainfall;
						++monthCounter;
					}
					else {
						System.out.println("Error. Rainfall cannot be less than 0.");
						break;
					}
				}	
			}
			
			double averageRainfall = (double)totalRainfall / (double)monthCounter;
			
			System.out.println("The number of months is: " + monthCounter);
			System.out.println("The total inches of rainfall is: " + totalRainfall);
			System.out.printf("The average rainfall for the entire period is: %.2f", averageRainfall);
			
			
		}
		else {
			System.out.println("Error. Number of years cannot be less than 1.");
		}
		
	}

}
