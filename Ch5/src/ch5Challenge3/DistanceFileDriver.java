package ch5Challenge3;

import java.io.*;
import java.util.Scanner;

public class DistanceFileDriver {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		
		boolean isValidated = true;
		
		System.out.println("Please enter the speed of the vehicle");
		double vehicleSpeed = keyboard.nextDouble();
		if(vehicleSpeed < 0) {
			System.out.println("Error, speed cannot be less than 0");
			isValidated = false;
		}
		System.out.println("Please enter the time travelled");
		int timeTravelled = keyboard.nextInt();
		if(timeTravelled < 1) {
			System.out.println("Error, time travelled cannot be less than 1");
			isValidated = false;
		}
		
		DistanceFile travel = new DistanceFile(vehicleSpeed, timeTravelled);
		
		PrintWriter outputFile = new PrintWriter("src\\ch5Challenge3\\DistanceTravelled.txt");
		
		if(isValidated = true) {
			for(int x = 1; x <= timeTravelled; ++x) {
				travel.setTime(x);
				System.out.println("Hour " + x + ":\n Distance Travelled: " + travel.getDistance());
				outputFile.println("Hour " + x + ":\n Distance Travelled: " + travel.getDistance());
				
				if(x == timeTravelled) {
					System.out.println("Your distance travelled has been converted into a text document.");
					outputFile.close();
				}
			}
		}
	}

}
