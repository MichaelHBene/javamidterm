package ch5Challenge1;

import java.util.Scanner;
public class SumOfNumbers {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Please enter an integer");
		int userInt = keyboard.nextInt();
		int total;
		
		for(int x = 1; x <= userInt; ++x) {
			total = x * userInt;
			System.out.println(x + " * " + userInt + " = " + total);
		}
	}

}
