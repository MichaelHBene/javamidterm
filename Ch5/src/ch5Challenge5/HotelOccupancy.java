package ch5Challenge5;

import java.util.Scanner;
public class HotelOccupancy {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("How many floors are in the hotel?:");
		Scanner keyboard = new Scanner(System.in);
		
		int numFloors = keyboard.nextInt();
		int numRooms = 0;
		int numRoomsOccupied = 0;
		boolean validated = true;
		if(numFloors >= 1) {
			for(int x = 1; x <= numFloors; ++x) {
				System.out.println("How many rooms are on floor #" + x + " ?:");
				numRooms += keyboard.nextInt();
				if(numRooms >= 10) {
					System.out.println("How many rooms are occupied?:");
					numRoomsOccupied += keyboard.nextInt();
				}
				else {
					System.out.println("Error. Floors cannot have less than 10 rooms.");
					validated = false;
					break;
				}
			}	
			
			if(validated) {
				double hotelOccupancy = ((double)numRoomsOccupied / (double)numRooms) * 100;
				int numRoomsVacant = numRooms - numRoomsOccupied;
				System.out.println("The number of rooms in the hotel is: " + numRooms);
				System.out.println("The number of rooms occupied is: " + numRoomsOccupied);
				System.out.println("The number of rooms vacant is: " + numRoomsVacant);
				System.out.println("The occupancy rate of the hotel is: " + hotelOccupancy + "%");		
			}
		
			
		}
		else {
			System.out.println("Error. Cannot have less than 1 floor.");
		}
		
	}

}
