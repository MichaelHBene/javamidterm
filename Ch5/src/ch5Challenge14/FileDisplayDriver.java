package ch5Challenge14;

import java.io.IOException;
import java.util.Scanner;
public class FileDisplayDriver {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub

		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Please enter the name of the file:");
		String fileName = keyboard.nextLine();
		
		FileDisplay file = new FileDisplay(fileName);
		
		System.out.println("Enter 1 to display the head of the document.\nEnter 2 to display the entire document.\nEnter 3 to display the entire document with numbered bullets:");
		int userChoice = keyboard.nextInt();
		
		switch(userChoice) {
		case 1:
			System.out.println("Here is the head:");
			file.displayHead();
			break;
		case 2:
			System.out.println("\nHere is the entire document:");
			file.displayContents();
			break;
		case 3:
			System.out.println("\nHere is the entire document numbered:");
			file.displayWithLineNumbers();
			break;
		default:
			System.out.println("Error. Choice not valid.");
		}
		
		
	}

}
