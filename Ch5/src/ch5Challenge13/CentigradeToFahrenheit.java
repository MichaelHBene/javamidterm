package ch5Challenge13;

public class CentigradeToFahrenheit {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Centigrate to Fahrenheit Conversion Table:\n==========================================");
		
		for(int x = 0; x <= 20; ++x) {
			double fahrenheit = (((double)9/(double)5) * (double)x) + 32;
			
			System.out.printf(x + " degrees Centigrade = %.1f degrees Fahrenheit.\n", fahrenheit);
		}
	}

}
