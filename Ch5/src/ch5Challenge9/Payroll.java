package ch5Challenge9;

public class Payroll {

	private int idNumber;
	private double grossPay;
	private double stateTax;
	private double federalTax;
	private double FICA;
	
	public Payroll(int id, double gross, double state, double fed, double withhold) {
		idNumber = id;
		grossPay = gross;
		stateTax = state;
		federalTax = fed;
		FICA = withhold;
	}

	public double getNetPay() {
		double netPay = grossPay - stateTax - federalTax - FICA;
		return netPay;
	}
	
}
