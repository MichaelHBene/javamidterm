package ch5Challenge12;

import java.util.Scanner;
public class BarChart {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner keyboard = new Scanner(System.in);
		
		System.out.println("Please enter today's sales for store #1:");
		int store1Sales = keyboard.nextInt();
		
		System.out.println("Please enter today's sales for store #2:");
		int  store2Sales = keyboard.nextInt();
		
		System.out.println("Please enter today's sales for store #3:");
		int  store3Sales = keyboard.nextInt();
		
		System.out.println("Please enter today's sales for store #4:");
		int  store4Sales = keyboard.nextInt();
		
		System.out.println("Please enter today's sales for store #5:");
		int  store5Sales = keyboard.nextInt();
		
		System.out.println("SALES BAR CHART\n===============");
		System.out.println("Store #1: " + getAsterisk(store1Sales));
		System.out.println("Store #1: " + getAsterisk(store2Sales));
		System.out.println("Store #1: " + getAsterisk(store3Sales));
		System.out.println("Store #1: " + getAsterisk(store4Sales));
		System.out.println("Store #1: " + getAsterisk(store5Sales));
		
	}
	
	public static String getAsterisk(int sales) {
		
		int numAsterisks = (sales/ 100);
		
		String asteriskBar = "";
		
		for(int x = 1; x <= numAsterisks; ++x) {
			asteriskBar += "*";
		}
		
		return asteriskBar;
		
	}

}
