package ch5Challenge10;

import java.util.Scanner;
public class SavingsAccountDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Please enter the starting balance of the account:");
		double startingBalance = keyboard.nextDouble();
		System.out.println("Please enter the annual interest rate of the account:");
		double interestRate = keyboard.nextDouble();
		System.out.println("Please enter the number of months that have passed since the account was established:");
		int months = keyboard.nextInt();
		
		SavingsAccount account = new SavingsAccount(startingBalance, interestRate);
		
		double totalDeposits = 0;
		double totalWithdrawls = 0;
		double totalInterest = 0;
		
		for(int x = 1; x <= months; ++x) {
			System.out.println("Please enter the amount deposited into the account this month:");
			double deposits = keyboard.nextDouble();
			totalDeposits += deposits;
			account.addBalance(deposits);
			
			System.out.println("Please enter the amount withdrawn from the account this month:");
			double withdrawls = keyboard.nextDouble();
			totalWithdrawls += withdrawls;
			account.subtractBalance(withdrawls);

			totalInterest += account.addInterest();
			
		}
		
		System.out.printf("The ending balance of the account is: $%.2f", account.getBalance());
		System.out.printf("\nThe total amount of deposits during this period was: $%.2f", totalDeposits);
		System.out.printf("\nThe total amount of withdrawls during this period was: $%.2f", totalWithdrawls);
		System.out.printf("\nThe total amount of interest earned during this period was: $%.2f", totalInterest);
	}

}
